import std;

version (unittest)
{
    import unit_threaded;
}

/+

  if a sensor with distance to beacon 9 is in row y, then the range s.x-dist...s.x-1 and s.x+1..s.x+dist is the number of impossible cells.
  sensor at 8,7 with distance 9:
    range(-1..17) in line 7 (needs to remove all sensors)
    range(0..16) in line 6 ()



 +/
struct Position
{
    int x;
    int y;
}
class Beacon
{
    Position position;
    this(int x, int y)
    {
        position = Position(x, y);
    }
}

int manhatten(Position p1, Position p2)
{
    return (p1.x-p2.x).abs + (p1.y-p2.y).abs;
}

class Sensor
{
    Position position;
    Beacon nearest;
    this(int x, int y, Beacon b)
    {
        position = Position(x, y);
        nearest = b;
    }
}

class Field
{
    string[Position] data;
    Position leftTop;
    Position rightBottom;

    static Field parse(string input)
    {
        Field result = new Field;
        foreach (m; input.split("\n").map!(l => l.matchFirst(regex("Sensor at x=(-?\\d+), y=(-?\\d+): closest beacon is at x=(-?\\d+), y=(-?\\d+)"))))
        {
            Beacon b = new Beacon(m[3].to!int, m[4].to!int);
            Sensor s = new Sensor(m[1].to!int, m[2].to!int, b);
            result.data[s.position] = "S";
            result.data[b.position] = "B";
            result.markEmpty(s);
            auto extremes = result.data.keys.reduce!(
              (a, b) => Position(min(a.x, b.x), min(a.y, b.y)),
              (a, b) => Position(max(a.x, b.x), max(a.y, b.y)),
            );
            result.leftTop = extremes[0];
            result.rightBottom = extremes[1];
            //writeln(result.toString);
        }
        return result;
    }
    void markEmpty(Sensor s)
    {
        int distance = s.position.manhatten(s.nearest.position);
        for (int j=0; j<=distance; ++j)
        {
            for (int i=0; i<=distance; ++i)
            {
                if (i + j <= distance)
                {
                    markEmpty(s.position.x - i, s.position.y -j);
                    markEmpty(s.position.x + i, s.position.y-j);
                    markEmpty(s.position.x - i, s.position.y+j);
                    markEmpty(s.position.x + i, s.position.y+j);
                }
            }
        }
    }
    void markEmpty(int x, int y)
    {
        Position i = Position(x, y);
        if ((i in data) is null)
        {
            data[i] = "#";
        }
    }
    
    override string toString()
    {
        string result;
        for (int j=leftTop.y; j<=rightBottom.y; j++)
        {
            for (int i=leftTop.x; i<=rightBottom.x; i++)
            {
                auto p = Position(i, j);
                if ((Position(i, j) in data) !is null)
                {
                    result ~= data[p];
                }
                else
                {
                    result ~= ".";
                }
            }
            result ~= "\n";
        }
        return result;
    }
    int countRow(int y, string what)
    {
        int result = 0;
        foreach (p; data.keys)
        {
            if (p.y == y)
            {
                if (data[p] == what)
                {
                    result++;
                }
            }
        }
        return result;
    }
}
auto exampleData()
{
    return `Sensor at x=2, y=18: closest beacon is at x=-2, y=15
Sensor at x=9, y=16: closest beacon is at x=10, y=16
Sensor at x=13, y=2: closest beacon is at x=15, y=3
Sensor at x=12, y=14: closest beacon is at x=10, y=16
Sensor at x=10, y=20: closest beacon is at x=10, y=16
Sensor at x=14, y=17: closest beacon is at x=10, y=16
Sensor at x=8, y=7: closest beacon is at x=2, y=10
Sensor at x=2, y=0: closest beacon is at x=2, y=10
Sensor at x=0, y=11: closest beacon is at x=2, y=10
Sensor at x=20, y=14: closest beacon is at x=25, y=17
Sensor at x=17, y=20: closest beacon is at x=21, y=22
Sensor at x=16, y=7: closest beacon is at x=15, y=3
Sensor at x=14, y=3: closest beacon is at x=15, y=3
Sensor at x=20, y=1: closest beacon is at x=15, y=3`;
}

auto realData()
{
    return File("input.txt").readFully;
}

@("example") unittest
{
    Field.parse(exampleData).countRow(10, "#").should == 26;
}

@("real") unittest
{
    writeln("part1");
    writeln(Field.parse(realData).toString);//countRow(2_000_000, "#").to!string);
    //"part1: %s".format(Field.parse(realData).countRow(2_000_000, "#")).writeln;
    //writeln("part2: %s".format("TODO"));
}

string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

auto eachCons(R)(R range, size_t length)
{
    struct EachCons
    {
        bool empty()
        {
            return range.length < length;
        }
        auto front()
        {
            return range[0..length];
        }
        void popFront()
        {
            range.popFront;
        }
    }
    return EachCons();
}

@("eachCons") unittest
{
    [1,2,3].eachCons(2).array.should == [[1, 2], [2, 3]];
    [1,2,3].eachCons(4).empty.should == true;
    [1,2,3].eachCons(4).array.empty.should == true;
}
