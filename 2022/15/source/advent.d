import std;

version (unittest)
{
    import unit_threaded;
}

auto exampleData()
{
    return `Sensor at x=2, y=18: closest beacon is at x=-2, y=15
Sensor at x=9, y=16: closest beacon is at x=10, y=16
Sensor at x=13, y=2: closest beacon is at x=15, y=3
Sensor at x=12, y=14: closest beacon is at x=10, y=16
Sensor at x=10, y=20: closest beacon is at x=10, y=16
Sensor at x=14, y=17: closest beacon is at x=10, y=16
Sensor at x=8, y=7: closest beacon is at x=2, y=10
Sensor at x=2, y=0: closest beacon is at x=2, y=10
Sensor at x=0, y=11: closest beacon is at x=2, y=10
Sensor at x=20, y=14: closest beacon is at x=25, y=17
Sensor at x=17, y=20: closest beacon is at x=21, y=22
Sensor at x=16, y=7: closest beacon is at x=15, y=3
Sensor at x=14, y=3: closest beacon is at x=15, y=3
Sensor at x=20, y=1: closest beacon is at x=15, y=3`;
}

auto realData()
{
    return File("input.txt").readFully;
}

int countThingsOnLine(T)(Interval[] intervals, int line, T[] things)
{
    int count = 0;
    foreach (thing; things)
    {
        auto pos = thing.position;
        if (pos.y != line)
        {
            continue;
        }
        foreach (i; intervals)
        {
            if (i.includes(pos.x))
            {
                count++;
            }
        }
    }
    return count;
}

int countInLine(Sensor[Position] sensors, Beacon[Position] beacons, int line)
{
    Interval[] intervals;
    foreach (sensor; sensors)
    {
        auto yDistance = abs(sensor.position.y - line);
        if (yDistance <= sensor.radius)
        {
            intervals ~= new Interval(sensor.position.x-sensor.radius+yDistance, (sensor.radius-yDistance)*2+1);
        }
    }
    auto merged = intervals.merge;
    return merged.map!("a.size").sum
        - merged.countThingsOnLine(line, beacons.values)
        - merged.countThingsOnLine(line, sensors.values);
}

@("example") unittest
{
    auto data = exampleData.parse;
    data.sensors.countInLine(data.beacons, 10).should == 26;
}

auto findSpotWithoutCoverage(Sensor[] sensors)
{
    foreach (sensor; sensors)
    {
        foreach (p; sensor.walkPerimeter(sensor.radius+1))
        {
            if (p.x < 0 || p.x > 4_000_000 || p.y < 0 || p.y > 4_000_000)
            {
                continue;
            }
            auto covered = false;
            foreach (s; sensors)
            {
                if (s != sensor)
                {
                    if (s.position.manhatten(p) <= s.radius)
                    {
                        covered = true;
                        break;
                    }
                }
            }
            if (!covered)
            {
                return p;
            }
        }
    }
    assert(0);
}

auto tuningFrequency(Position p)
{
    long l = p.x;
    return l * 4_000_000 + p.y;
}

@("real") unittest
{
    auto data = realData.parse;
    auto part1 = data.sensors.countInLine(data.beacons, 2_000_000);
    part1.should == 5525847;
    "part1: %s".format(part1).writeln;
    auto part2 = data.sensors.values.findSpotWithoutCoverage.tuningFrequency;
    part2.should == 13340867187704;
    "part2: %s".format(part2).writeln;
}

struct Position
{
    int x;
    int y;
}
class Beacon
{
    Position position;
    this(int x, int y)
    {
        position = Position(x, y);
    }
}

int manhatten(Position p1, Position p2)
{
    return (p1.x-p2.x).abs + (p1.y-p2.y).abs;
}

class Sensor
{
    Position position;
    Beacon nearest;
    int radius;
    this(int x, int y, Beacon b)
    {
        position = Position(x, y);
        nearest = b;
        radius = b.position.manhatten(position);
    }

    auto walkPerimeter(int radius)
    {
        auto p = position;
        struct Perimeter
        {
            Position start;
            Position current;
            int dx;
            int dy;
            bool first;
            this(int radius)
            {
                start = Position(p.x-radius, p.y);
                current = Position(p.x-radius, p.y);
                this.dx = 1;
                this.dy = -1;
                first = true;
            }
            bool empty()
            {
                return (first == false) && (current == start);
            }
            void popFront()
            {
                current = Position(current.x + dx, current.y +dy);
                first = false;
                if (current.x == p.x+radius)
                {
                    dx = -1;
                }
                if (current.y == p.y - radius)
                {
                    dy = 1;
                }
                if (current.y == p.y+radius)
                {
                    dy = -1;
                }
            }
            Position front()
            {
                return current;
            }
        }
        return Perimeter(radius);
    }
}

@("perimeter") unittest
{
    auto sensor = new Sensor(0, 0, new Beacon(5, 0));
    sensor.walkPerimeter(3).count.should == 12;
}

auto parse(string input)
{

    Sensor[Position] sensors;
    Beacon[Position] beacons;
    foreach (theMatch; input.strip.split("\n").map!(l => l.matchFirst(regex("Sensor at x=(-?\\d+), y=(-?\\d+): closest beacon is at x=(-?\\d+), y=(-?\\d+)"))))
    {
        Beacon b = new Beacon(theMatch[3].to!int, theMatch[4].to!int);
        Sensor s = new Sensor(theMatch[1].to!int, theMatch[2].to!int, b);
        beacons[b.position] = b;
        sensors[s.position] = s;
    }
    return tuple!("sensors", "beacons")(sensors, beacons);
}


string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

auto eachCons(R)(R range, size_t length)
{
    struct EachCons
    {
        bool empty()
        {
            return range.length < length;
        }
        auto front()
        {
            return range[0..length];
        }
        void popFront()
        {
            range.popFront;
        }
    }
    return EachCons();
}

@("eachCons") unittest
{
    [1,2,3].eachCons(2).array.should == [[1, 2], [2, 3]];
    [1,2,3].eachCons(4).empty.should == true;
    [1,2,3].eachCons(4).array.empty.should == true;
}

class Interval
{

    int start;
    int count;
    this(int start, int count)
    {
        this.start = start;
        this.count = count;
    }
    auto setEnd(int end)
    {
        count = end-start+1;
    }
    int size()
    {
        return count;
    }
    /++ Returns the end of the interval (the last int included in the interval) +/
    int end()
    {
        return start+count-1;
    }

    override string toString()
    {
        return "Interval[%s, %s]".format(start, end);
    }
    bool includes(int v)
    {
        return v >= start && v <= end;
    }
}

@("basic interval operations") unittest
{
    auto i1 = new Interval(1, 5);
    i1.end.should == 5;
    i1.size.should == 5;
    i1.setEnd(10);
    i1.end.should == 10;
    i1.size.should == 10;
}

auto merge(Interval[] intervals)
{
    Interval[] result;
    intervals.sort!("a.start < b.start");
    foreach (i; intervals)
    {
        if (result.empty)
        {
            result ~= i;
        }
        else
        {
            if (result.back.end < i.start)
            {
                result ~= i;
            }
            else
            {
                result.back.setEnd(max(result.back.end, i.end));
            }
        }
    }
    return result;
}

@("interval merge") unittest
{
    [new Interval(1,5), new Interval(2, 6), new Interval(10, 5)].merge.should == [new Interval(1,7), new Interval(10, 5)];
}
