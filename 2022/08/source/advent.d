import std;


ubyte[][] trees;

auto width(ubyte[][] trees)
{
    return trees[0].length;
}

auto height(ubyte[][] trees)
{
    return trees.length;
}

bool inRange(ubyte[][] trees, int x, int y)
{
    if (x < 0)
    {
        return false;
    }
    if (y < 0)
    {
        return false;
    }
    if (y >= trees.height)
    {
        return false;
    }
    if (x >= trees.width)
    {
        return false;
    }
    return true;
}

bool anyBigger(ubyte[][] trees, int x, int y, int dx, int dy)
{
    int hx = x + dx;
    int hy = y + dy;
    while (inRange(trees, hx, hy))
    {
        if (trees[hy][hx] >= trees[y][x])
        {
            return true;
        }
        hx += dx;
        hy += dy;
    }
    return false;
}

bool visible(ubyte[][] trees, int x, int y)
{
    if (x == 0)
    {
        return true;
    }
    if (y == 0)
    {
        return true;
    }
    if (x == trees.width-1)
    {
        return true;
    }
    if (y == trees.height -1)
    {
        return true;
    }
    if (trees.anyBigger(x, y, -1, 0)
        && trees.anyBigger(x, y, 1, 0)
        && trees.anyBigger(x, y, 0, -1)
        && trees.anyBigger(x, y, 0, 1))
    {
        return false;
    }
    return true;
}

ulong score(ubyte[][] trees, int x, int y, int dx, int dy)
{
    ulong score = 0;
    int hx = x + dx;
    int hy = y + dy;
    while (trees.inRange(hx, hy))
    {
        score++;
        if (trees[hy][hx] >= trees[y][x])
        {
            return score;
        }
        hx += dx;
        hy += dy;
    }
    return score;
}
ulong score(ubyte[][] trees, int x, int y)
{
    return
        trees.score(x, y, 1, 0)
        * trees.score(x, y, -1, 0)
        * trees.score(x, y, 0, 1)
        * trees.score(x, y, 0, -1);
}

version (unittest)
{
}
else
{
    int main(string[] args)
    {
        foreach (line; stdin.byLineCopy)
        {
            trees ~= cast(ubyte[])line;
        }
        ulong visibleTrees = 0;
        ulong score = 0;
        for (int j=0; j<trees.height; ++j)
        {
            for (int i=0; i<trees.width; ++i)
            {
                if (trees.visible(i, j))
                {
                    visibleTrees++;
                }
                score = max(score, trees.score(i, j));
            }
        }

        writeln("visibleTrees: ", visibleTrees);
        writeln("best Score: ", score);

        return 0;
    }
}
