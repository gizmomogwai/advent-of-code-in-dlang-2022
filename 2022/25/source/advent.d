import std;

version (unittest)
{
    import unit_threaded;
}

auto exampleData()
{
    return `1=-0-2
12111
2=0=
21
2=01
111
20012
112
1=-1=
1-12
12
1=
122
`;
}

auto realData()
{
    return File("input.txt").readFully;
}

long snafuchar2long(dchar c)
{
    final switch (c)
    {
    case '2':
        return 2;
    case '1':
        return 1;
    case '0':
        return 0;
    case '-':
        return -1;
    case '=':
        return -2;
    }
}

long snafustring2long(string s)
{
    return reduce!((acc, v) { return acc * 5 + snafuchar2long(v); })(cast(long)0, s);
}

auto toSnafu(long v)
{
    string result;
    while (v != 0)
    {
        final switch (v % 5)
        {
        case 0:
            result ~= '0';
            break;
        case 1:
            result ~= '1';
            break;
        case 2:
            result ~= '2';
            break;
        case 3:
            result ~= '=';
            v += 2;
            break;
        case 4:
            result ~= '-';
            v += 1;
            break;
        }
        v /= 5;
    }
    return result.array.reverse;
}

@("example") unittest
{
    1.toSnafu.should == "1";
    2             .toSnafu.should == "2";
    3             .toSnafu.should == "1=";
    4             .toSnafu.should == "1-";
    5             .toSnafu.should == "10";
    6             .toSnafu.should == "11";
    7             .toSnafu.should == "12";
    8             .toSnafu.should == "2=";
    9             .toSnafu.should == "2-";
    10             .toSnafu.should == "20";
    15            .toSnafu.should == "1=0";
    20            .toSnafu.should == "1-0";
    2022         .toSnafu.should == "1=11-2";
    12345        .toSnafu.should == "1-0---0";
    314159265  .toSnafu.should == "1121-1110-1=0";
    exampleData.strip.split("\n").map!(snafustring2long).sum.toSnafu.should == "2=-1=0";
}

@("real") unittest
{
    import std.stdio : writeln;

    writeln("part1: %s".format(realData.strip.split("\n").map!(snafustring2long).sum.toSnafu));

    writeln("part2: %s".format("TODO"));
}

string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

auto eachCons(R)(R range, size_t length)
{
    struct EachCons
    {
        bool empty()
        {
            return range.length < length;
        }
        auto front()
        {
            return range[0..length];
        }
        void popFront()
        {
            range.popFront;
        }
    }
    return EachCons();
}

@("eachCons") unittest
{
    [1,2,3].eachCons(2).array.should == [[1, 2], [2, 3]];
    [1,2,3].eachCons(4).empty.should == true;
    [1,2,3].eachCons(4).array.empty.should == true;
}
