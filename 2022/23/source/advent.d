import std;
import optional;

struct Pos {
    int x, y;
    Pos opBinary(string op)(Pos o){
        return mixin("Pos(x", op, "o.x,y", op, "o.y)");
    }
    ref pos opOpAssign(string op)(Pos o){
        return this = this.opBinary!op(o);
    }
}


version (unittest)
{
    import unit_threaded;
}

auto exampleData()
{
    /+
    return `.....
..##.
..#..
.....
..##.
.....`;
+/
     return `..............
     ..............
     .......#......
     .....###.#....
     ...#...#.#....
     ....#...##....
     ...#.###......
     ...##.#.##....
     ....#..#......
     ..............
     ..............
     ..............`;
}

auto parse(string s)
{
    Elv[Pos] elves;
    foreach (j, line; s.strip.split("\n"))
    {
        foreach (i, c; line)
        {
            if (c == '#')
            {
                elves[Pos(cast(int)i, cast(int)j)] = new Elv();
            }
        }
    }
    return elves;
}

struct Proposal
{
    Pos[] moveAndNeighbors;
    Pos[] points(Pos pos)
    {
        return moveAndNeighbors.map!(p => p + pos).array;
    }
}

auto proposals = [
  Proposal([Pos(0, -1), Pos(-1, -1), Pos(1, -1)]),
  Proposal([Pos(0, 1), Pos(-1, 1), Pos(1, 1)]),
  Proposal([Pos(-1, 0), Pos(-1, -1), Pos(-1, 1)]),
  Proposal([Pos(1, 0), Pos(1, -1), Pos(1, 1)]),
].cycle;

auto realData()
{
    return File("input.txt").readFully;
}

auto neighbors(Pos p)
{
    struct N
    {
        int i = 0;
        static Pos[] deltas = [Pos(-1, -1), Pos(0, -1), Pos(1, -1), Pos(-1, 0), Pos(1, 0), Pos(-1, 1), Pos(0, 1), Pos(1, 1)];
        auto front()
        {
            return p+deltas[i];
        }
        bool empty()
        {
            return i == deltas.length;
        }
        void popFront()
        {
            i++;
        }
    }
    return N();
}

class Elv
{
    bool wantsToMove;
    Optional!Proposal proposal;
    Pos newPos;
    this()
    {
        wantsToMove = false;
        proposal = none;
    }
}

Optional!(ElementType!R) findFirst(alias pred, R)(R range)
{
    foreach (r; range)
    {
        if (binaryFun!(pred)(r))
        {
            return some(r);
        }
    }
    return no!(ElementType!(R));
}

void print(int[Pos] propositions)
{
    auto d = propositions.dimension;
    for (int j=d[0].y; j<=d[1].y; j++)
    {
        for (int i=d[0].x; i<=d[1].x; i++)
        {
            auto p = Pos(i, j);
            auto h = p in propositions;
            if (h !is null)
            {
                write(propositions[p]);
            }
            else
            {
                write(" ");
            }
        }
        writeln;
    }
}
auto run(ref Elv[Pos] elves, int rounds=1)
{

    int[Pos] props;
    for (int round=0; round<rounds; ++round)
    {
        auto possibleMoves = proposals.take(4).array;
        proposals.popFront;

        props.clear;

        foreach (pos, elv; elves)
        {
            elv.wantsToMove = pos.neighbors.any!(i => (i in elves) !is null);
        }

        foreach (pos, elv; elves)
        {
            if (elv.wantsToMove)
            {
                auto h = possibleMoves.findFirst!(p => p.points(pos).all!(i => (i in elves) is null));
                if (h.empty)
                {
                    elv.proposal = none;
                }
                else
                {
                    elv.proposal = h.front.some;
                    elv.newPos = elv.proposal.front.moveAndNeighbors[0] + pos;
                    props.update(
                      elv.newPos,
                      () => 1,
                      (ref int v) => v += 1,
                    );
                }
            }
        }

//    print(props);
        int count = 0;
        foreach (pos, elv; elves)
        {
            if (elv.wantsToMove)
            {
                if (!elv.proposal.empty)
                {
                    if (props[elv.newPos] == 1)
                    {
                        elves.remove(pos);
                        elves[elv.newPos] = elv;
                        count++;
                    }
                }
            }
        }
        if (count == 0)
        {
            return round+1;
        }
    }
    return rounds;
}

auto dimension(A)(A[Pos] elves)
{
    return elves.keys.fold!(
      (a, b) => Pos(min(a.x, b.x), min(a.y, b.y)),
      (a, b) => Pos(max(a.x, b.x), max(a.y, b.y)),
    );
}

void print(Elv[Pos] elves)
{
    auto dimension = elves.dimension;
    for (int j=dimension[0].y; j<=dimension[1].y; j++)
    {
        write(j, ":");
        for (int i=dimension[0].x; i<=dimension[1].x; ++i)
        {
            if ((Pos(i, j) in elves) !is null)
            {
                write("#");
            }
            else
            {
                write(".");
            }
        }
        writeln;
    }
}

auto countEmpty(Elv[Pos] elves)
{
    auto d = elves.dimension;
    return (d[1].y - d[0].y+1) * (d[1].x-d[0].x+1) - elves.length;
}

@("example") unittest
{
    auto elves = exampleData.parse;
    elves.length.should == 22;
    elves.run(10);
    elves.countEmpty.should == 110;
    auto h = elves.run(1000);
    h.should == 10;
}

@("real") unittest
{
    auto elves = realData.parse;
    writeln(elves.length);
    elves.run(10);
    elves.countEmpty.should == 4052;
    writeln("part1: %s".format(elves.countEmpty));
    auto h = elves.run(100000);
    h.should == (978-10);
    writeln("part2: %s".format(h+10));
}


string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

auto eachCons(R)(R range, size_t length)
{
    struct EachCons
    {
        bool empty()
        {
            return range.length < length;
        }
        auto front()
        {
            return range[0..length];
        }
        void popFront()
        {
            range.popFront;
        }
    }
    return EachCons();
}

@("eachCons") unittest
{
    [1,2,3].eachCons(2).array.should == [[1, 2], [2, 3]];
    [1,2,3].eachCons(4).empty.should == true;
    [1,2,3].eachCons(4).array.empty.should == true;
}

void dfs(I)(void delegate(ref I[]) initial, void delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    queue.initial;
    while (!queue.empty)
    {
        auto i = queue.back;
        queue.popBack;
        i.visit(queue);
        i.visited;
    }
}

void bfs(I)(void delegate(ref I[]) initial, bool delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    initial(queue);
    while (!queue.empty)
    {
        auto i = queue.front;
        queue.popFront;
        bool found = visit(i, queue);
        visited(i);
        if (found)
        {
            break;
        }
    }
}
