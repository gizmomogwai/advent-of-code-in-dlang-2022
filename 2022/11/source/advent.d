import std;

string example()
{
return
`Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1
`;
}

auto run(T)(T monkeys, int rounds, long modulus=0)
{
    for (int i=0; i<rounds; ++i)
    {
        foreach (monkey; monkeys)
        {
            monkey.inspect(monkey.items.length);
            monkey.update(monkeys, modulus);
        }
    }
    auto busy = monkeys.sort!((a, b) { return a.inspected > b.inspected;}).take(2).array;
    return busy[0].inspected.to!BigInt * busy[1].inspected;
}

@("lcm") unittest
{
    import unit_threaded;
    [2, 3, 5, 6].reduce!(lcm).should == 30;
}

@("example") unittest
{
    import unit_threaded;

    example.parse.run(20).should == 10605;

    auto monkeys = example.parse;
    auto modulus = monkeys.map!(m => m.div).reduce!((result, x) {return result * x;});
    example.parse.run(10_000, modulus).should == 2713310158;
}
string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

class Monkey(T)
{
    long[] items;
    T op;
    int div;
    int t;
    int f;
    int inspected;
    this(long[] items, T op, int div, int t, int f)
    {
        this.items = items;
        this.op = op;
        this.div = div;
        this.t=t;
        this.f=f;
    }
    void inspect(ulong n)
    {
        inspected += n;
    }
    void update(Monkey!(T)[] monkeys, long modulus)
    {
        foreach (i; items)
        {
            long newI = i;
            switch (op[0])
            {
            case "*":
                if (op[1] == "old")
                {
                    newI = i * i;
                }
                else
                {
                    newI = i * op[1].to!long;
                }
                break;
            case "+":
                newI = i + op[1].to!long;
                break;
            default:
                throw new Exception("Unknow op " ~ op[0]);
            }

            if (modulus == 0)
            {
                newI /= 3;
            }
            else
            {
                newI %= modulus;
            }
            long next = newI % div == 0 ? t : f;
            monkeys[next].items ~= newI;
            items = [];
        }
    }
    override string toString()
    {
        return "Monkey (" ~ items.map!(i => i.to!string).join(", ") ~ ") inspected " ~ (inspected.to!string);
    }
}
auto monkey(T)(long[] items, T op, int div, int t, int f)
{
    return new Monkey!(T)(items, op, div, t, f);
}
auto parseMonkey(string s)
{
    writeln("parseMonkey: '", s, "'");
    auto items = s.matchFirst(regex("Starting items: (.+)$", "m"))[1].split(", ").map!(s => s.to!long).array;
    auto m = s.matchFirst(regex("Operation: new = old (\\*|\\+) (.+)$", "m"));
    auto op = tuple(m[1], m[2]);
    auto div = s.matchFirst(regex("Test: divisible by (\\d+)$", "m"))[1].to!int;
    auto t = s.matchFirst(regex("If true: throw to monkey (\\d)$", "m"))[1].to!int;
    auto f = s.matchFirst(regex("If false: throw to monkey (\\d)$", "m"))[1].to!int;
    return monkey(items, op, div, t, f);
}
auto parse(string input)
{
    return input.split("\n\n").filter!(s => s != "").map!(m => m.parseMonkey).array;
}

version (unittest)
{
}
else
{
    int main(string[] args)
    {
        string input = stdin.readFully;
        input.parse.run(20).writeln;

        auto monkeys = input.parse;
        auto modulus = monkeys.map!(m => m.div).reduce!((result, x) {return result * x;});
        monkeys.run(10_000, modulus).writeln;
        return 0;
    }
}
