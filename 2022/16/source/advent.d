import std;

version (unittest)
{
    import unit_threaded;
}

auto exampleData()
{
    return `Valve AA has flow rate=0; tunnels lead to valves DD, II, BB
Valve BB has flow rate=13; tunnels lead to valves CC, AA
Valve CC has flow rate=2; tunnels lead to valves DD, BB
Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE
Valve EE has flow rate=3; tunnels lead to valves FF, DD
Valve FF has flow rate=0; tunnels lead to valves EE, GG
Valve GG has flow rate=0; tunnels lead to valves FF, HH
Valve HH has flow rate=22; tunnel leads to valve GG
Valve II has flow rate=0; tunnels lead to valves AA, JJ
Valve JJ has flow rate=21; tunnel leads to valve II`;
}

class Valve
{
    string id;
    int rate;
    string[] stringConnections;
    Valve[string] connections;
    int index;
    int workingIndex;
    private static Valve[string] map;
    private static Valve[] workingValves;
    private static int staticIndex = 0;
    this(string id, int rate, string[] stringConnections)
    {
        this.id = id;
        this.rate = rate;
        this.stringConnections = stringConnections;
        this.index = staticIndex++;
        this.workingIndex = workingValves.length;
        map[id] = this;
        if (rate != 0)
        {
            workingValves ~= this;
        }
    }
    public static void updateConnections()
    {
        foreach (v; map.values)
        {
            foreach (s; v.stringConnections)
            {
                v.connections[s] = map[s];
            }
        }
    }

    public static auto get(string id)
    {
        return map[id];
    }

    override string toString()
    {
        return "Valve(%s, %s, %s)".format(id, rate, stringConnections);
    }
}
auto parseValve(string s)
{
    enum r = ctRegex!("Valve (?P<id>..) has flow rate=(?P<rate>\\d+); tunnels? leads? to valves? (?P<connections>.+)");
    auto m = s.matchFirst(r);
    if (m.empty)
    {
        throw new Exception("Cannot parse '" ~ s ~ "'");
    }
    return new Valve(m["id"], m["rate"].to!int, m["connections"].split(", "));
}

auto parse(string s)
{
    auto result = s.strip.split("\n").map!(l => l.parseValve).array;
    Valve.updateConnections;
    return result;
}

auto floydWarshal(T)(T input)
{
    auto l = input.length;
    float[][] distances = new float[][](l, l);
    for (int i=0; i<l; ++i)
    {
        for (int j=0; j<l; ++j)
        {
            if (i == j)
            {
                distances[i][j] = 0;
            }
            else if (input[j].id in input[i].connections)
            {
                distances[i][j] = 1;
            }
            else
            {
                distances[i][j] = float.infinity;
            }
        }
    }
    for (int k=0; k<l; ++k)
    {
        for (int j=0; j<l; ++j)
        {
            for (int i=0; i<l; ++i)
            {
                distances[i][j] = min(distances[i][j], distances[i][k]+distances[k][j]);
            }
        }
    }
    return distances.to!(int[][]);
}
void printDot(T)(string context, T input, int[][] distances)
{
    auto f = File(context ~ ".original.dot", "wb");
    f.writeln("digraph {");
    f.writeln("rankdir=\"LR\"");

    foreach (valve; input)
    {
        f.writeln(valve.id, "[label=\"", valve.id, "\n", valve.rate, "\"];");
        foreach (c; valve.connections)
        {
            f.writeln(valve.id, " -> ", c.id, ";");
        }
    }
    for (int i=0; i<input.length; ++i)
    {
        for (int j=0; j<input.length; ++j)
        {
            if (i < j)
            {
                f.writeln(input[i].id, " -> ", input[j].id, "[constraint=false, label=\"", distances[i][j], "\"];");
            }
        }
    }
    f.writeln("}");
//    writeln("from aa to dd: ", distances[Valve.get("AA").index][Valve.get("DD").index]);
}

struct State
{
    /// stores valves that are not yet visited
    BitArray notYetVisited;
    /// Name of current position
    string position;
    this(BitArray notYetVisited, string pos)
    {
        this.notYetVisited = notYetVisited;
        this.position = position;
    }
}
auto part1(T)(T input, string context="realData")
{
    auto distances = input.floydWarshal();
    //printDot(context, input, distances);
    bfs!State(
      (ref State[] queue)
      {
          auto notYetVisited = BitArray(true.repeat(Valve.workingValves.length).array);
          queue ~= State(notYetVisited, "AA");
      },
      (ref State v, ref State[] queue)
      {
          auto s = Valve.map[v.position];
          foreach (key, value; s.connections)
          {
              if (value.rate != 0 && v.notYetVisited[s.workingIndex])
              {
                  queue ~= State(
              }
          }
          return false;
      },
      (ref State)
      {
      },
    );
    return distances;
}

auto realData()
{
    return File("input.txt").readFully;
}

@("example") unittest
{
    float f = float.infinity;

    exampleData.parse.part1("example").writeln;
}

@("real") unittest
{
    import std.stdio : writeln;

    realData.parse.part1.writeln;
    writeln("part1: %s".format("TODO"));

    writeln("part2: %s".format("TODO"));
}

string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

auto eachCons(R)(R range, size_t length)
{
    struct EachCons
    {
        bool empty()
        {
            return range.length < length;
        }
        auto front()
        {
            return range[0..length];
        }
        void popFront()
        {
            range.popFront;
        }
    }
    return EachCons();
}

@("eachCons") unittest
{
    [1,2,3].eachCons(2).array.should == [[1, 2], [2, 3]];
    [1,2,3].eachCons(4).empty.should == true;
    [1,2,3].eachCons(4).array.empty.should == true;
}

void dfs(I)(void delegate(ref I[]) initial, void delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    queue.initial;
    while (!queue.empty)
    {
        auto i = queue.back;
        queue.popBack;
        i.visit(queue);
        i.visited;
    }
}

void bfs(I)(void delegate(ref I[]) initial, bool delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    initial(queue);
    while (!queue.empty)
    {
        auto i = queue.front;
        queue.popFront;
        bool found = visit(i, queue);
        visited(i);
        if (found)
        {
            break;
        }
    }
}
