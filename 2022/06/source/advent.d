import std;

int algo(string s, int count)
{
    auto h = s.array;
    for (int idx=count; idx<h.length; idx++)
    {
        auto part = h[idx-count..idx].dup.sort.uniq.array;
        if (part.length == count)
        {
            return idx+1;
        }
    }
    throw new Exception("not found");
}


@("examples") unittest {
    import unit_threaded;
    "mjqjpqmgbljsphdztnvjfqwrcgsmlb".algo(4).should == 7;
    "mjqjpqmgbljsphdztnvjfqwrcgsmlb".algo(14).should == 19;
    "bvwbjplbgvbhsrlpgdmjqwftvncz".algo(4).should == 5;
    "bvwbjplbgvbhsrlpgdmjqwftvncz".algo(14).should == 23;
    "nppdvjthqldpwncqszvftbrmjlhg".algo(4).should == 6;
    "nppdvjthqldpwncqszvftbrmjlhg".algo(14).should == 23;
    "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg".algo(4).should == 10;
    "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg".algo(14).should == 29;
    "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw".algo(4).should == 11;
    "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw".algo(14).should == 26;
}

string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

version(unittest)
{
}
else
{
    int main(string[] args)
    {
        auto s = stdin
            .readFully;
        s.algo(4).writeln;
        s.algo(14).writeln;
        return 0;
    }
}
