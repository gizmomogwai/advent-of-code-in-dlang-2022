import std;
import optional;

string example()
{
    return
`Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi`;
}

class Position
{
    int x;
    int y;
    this(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    bool equals(Position other)
    {
        return other.x == this.x && other.y == this.y;
    }
    Position move(Field field, int[] delta)
    {
        auto result = new Position(x+delta[0], y+delta[1]);
        if (result.x < 0)
        {
            return null;
        }
        if (result.y < 0)
        {
            return null;
        }
        if (result.x >= field.width)
        {
            return null;
        }
        if (result.y >= field.height)
        {
            return null;
        }
        if (field.get(result) - field.get(this) > 1)
        {
            return null;
        }
        return result;
    }
    override string toString()
    {
        return "Position(%s,%s)".format(this.x, this.y);
    }
}
bool[][] deepCopy(bool[][] input)
{
    bool[][] result;
    foreach (row; input)
    {
        result ~= row.dup;
    }
    return result;
}
class Field
{
    Position start;
    Position end;
    char[][] data;
    auto width()
    {
        return data[0].length;
    }
    auto height()
    {
        return data.length;
    }
    class Search
    {
        Position position;
        int distance;
        this(Position pos, int distance)
        {
            this.position = pos;
            this.distance = distance;
        }
    }
    Optional!int search(Position start, Position end)
    {
        bool[][] visited = new bool[][](height, width);
        Search[] queue;
        queue ~= new Search(start, 0);
        visited[start.y][start.x] = true;

        while (!queue.empty)
        {
            auto h = queue.front;
            queue.popFront;

            if (h.position.equals(end))
            {
                return some(h.distance);
            }
            foreach (nextCandidate; [[1,0], [-1,0], [0,1], [0,-1]].map!(delta => h.position.move(this, delta)))
            {
                if (nextCandidate is null)
                {
                    continue;
                }
                if (!visited[nextCandidate.y][nextCandidate.x])
                {
                    queue ~= new Search(nextCandidate, h.distance + 1);
                    visited[nextCandidate.y][nextCandidate.x] = true;
                }
            }
        }
        return no!int;
    }
    auto get(Position p)
    {
        return get(p.x, p.y);
    }
    auto get(int x, int y)
    {
        return data[y][x];
    }
}

Field parse(string[] lines)
{
    auto result = new Field();
    result.data = new char[][](lines.length, lines[0].length);
    foreach (j, line; lines)
    {
        foreach (i, c; line)
        {
            result.data[j][i] = c;
        }
    }
    for (int j=0; j<result.height; ++j)
    {
        for (int i=0; i<result.width; ++i)
        {
            if (result.data[j][i] == 'S')
            {
                result.data[j][i] = 'a';
                result.start = new Position(i, j);
            }
            else if (result.data[j][i] == 'E')
            {
                result.data[j][i] = 'z';
                result.end = new Position(i, j);
            }
        }
    }
    return result;
}
@("example") unittest
{
    import unit_threaded;
    auto field = example.split("\n").parse;
    field.width.should == 8;
    field.height.should == 5;
    field.start.x.should == 0;
    field.start.y.should == 0;
    field.end.x.should == 5;
    field.end.y.should == 2;

    auto result = field.search(field.start, field.end);
    result.front.should == 31;
}


auto enumerate(T)(T[][] array)
{
    const h = array.length;
    const w = array[0].length;
    struct Enumerate
    {
        int x;
        int y;
        bool empty()
        {
            return y >= h || x >= w;
        }
        auto front()
        {
            return tuple!("x", "y", "data")(x, y, array[y][x]);
        }
        void popFront()
        {
            x++;
            if (x >= w)
            {
                x = 0;
                y++;
            }
        }
    }
    return Enumerate();
}


version (unittest)
{
}
else
{
    int main(string[] args)
    {
        auto field = stdin
            .byLineCopy
            .array.parse;

        field.search(field.start, field.end).front.writeln;

        field
            .data
            .enumerate
            .map!(t => t.data == 'a' ? field.search(new Position(t.x, t.y), field.end) : no!int)
            .filter!(i => !i.empty)
            .map!(i => i.front)
            .minElement
            .writeln;

        return 0;
    }
}
