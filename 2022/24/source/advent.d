import std;

version (unittest)
{
    import unit_threaded;
}

auto exampleData()
{
    return `#.######
#>>.<^<#
#.<..<<#
#>v.><>#
#<^v^^>#
######.#`;
}

struct P
{
    int x;
    int y;

    auto add(Direction d)
    {
        return P(x + d.dx, y + d.dy);
    }
    auto addPlayer(Direction d)
    {
        return P(x + d.dx, y + d.dy);
    }

    auto clip(int width, int height)
    {
        int newX = x;
        int newY = y;

        if ((newX == 1 && newY == 0) || (newX == width-2 && newY == height-1))
        {
            return this;
        }
        if (newX < 1)
        {
            newX = width-2;
        }
        else if (newX >= width-1)
        {
            newX = 1;
        }
        if (newY < 1)
        {
            newY = height -2;
        }
        else if (newY >= height-1)
        {
            newY = 1;
        }
        return P(newX, newY);
    }
    bool legal(Field f)
    {
        if (((x == 1) && (y == 0)) || ((x == f.width-2) && (y == f.height-1)))
        {
            return true;
        }
        if ((x < 1) || (x >= f.width-1) || (y <1) || (y >= f.height-1))
        {
            return false;
        }
        return f.empty(x, y);
    }
    bool legalPlayer(Field f)
    {
        if ((x == 1 && y==0) || (x == f.width-2 && y == f.height-1))
        {
            return true;
        }
        if ((x < 1) || (y < 1) || (x >= f.width-1) || (y >= f.height-1))
        {
            return false;
        }
        return f.empty(x, y);
    }
}

struct Direction
{
    int dx;
    int dy;
}


struct Blizzard
{
    P pos;
    Direction dir;
}

auto realData()
{
    return File("input.txt").readFully;
}

struct Player
{
    P pos;
    int minute;
}
class Field
{
    Blizzard[] blizzards;
    bool[P] blizz;
    bool[P] borders;
    int width;
    int height;

    auto end()
    {
        return P(width-2, height-1);
    }
    auto start()
    {
        return P(1, 0);
    }

    void advance()
    {
        blizz.clear;
        foreach (ref b; blizzards)
        {
            b.pos = b.pos.add(b.dir).clip(width, height);
            blizz[b.pos] = true;
        }
    }

    bool empty(int x, int y)
    {
        auto p = P(x, y);
        if ((p in borders) !is null)
        {
            return false;
        }
        if ((p in blizz) !is null)
        {
            return false;
        }
        return true;
    }

    override string toString()
    {
        string res;
        res ~= "Field %sx%s\n".format(width, height);
        for (int j=0; j<height; ++j)
        {
            for (int i=0; i<width; ++i)
            {
                if ((P(i,j) in borders) !is null)
                {
                    res ~= "#";
                    continue;
                }
                bool blizzardFound = false;
                foreach (b; blizzards)
                {
                    if (b.pos.x == i && b.pos.y == j)
                    {
                        res ~= "b";
                        blizzardFound = true;
                        break;
                    }
                }
                if (!blizzardFound)
                {
                    res ~= ".";
                }
            }
            res ~= "\n";
        }
        return res;
    }
}

Direction[] directions = [Direction(1, 0), Direction(-1, 0), Direction(0, 1), Direction(0, -1), Direction(0, 0)];
auto parse(string s)
{
    Field result = new Field;
    foreach (j, line; s.strip.split("\n"))
    {
        result.width = cast(int)line.length;
        foreach (i, e; line)
        {
            auto p = P(cast(int)i, cast(int)j);
            final switch (e) {
            case '>':
                result.blizzards ~= Blizzard(p, Direction(1, 0));
                break;
            case '^':
                result.blizzards ~= Blizzard(p, Direction(0, -1));
                break;
            case '<':
                result.blizzards ~= Blizzard(p, Direction(-1, 0));
                break;
            case 'v':
                result.blizzards ~= Blizzard(p, Direction(0, 1));
                break;
            case '.':
                break;
            case '#':
                result.borders[p] = true;
                break;
            }
        }
        result.height = cast(int)j+1;
    }

    return result;
}

auto findExit(Field field, P start, P end)
{
    int minute = 0;
    bool[Player] alreadyLookingInto;
    bfs!(Player)(
      (ref Player[] queue)
      {
          // we start at 1, 0
          queue ~= Player(start, 1);
      },
      (ref Player player, ref Player[] queue)
      {
          if (player.minute != minute)
          {
              field.advance;
              alreadyLookingInto.clear;
              minute = player.minute;
          }

          foreach (d; directions)
          {
              auto newPos = player.pos.addPlayer(d);
              if (newPos.x == end.x && newPos.y == end.y)
              {
                  return true;
              }
              if (newPos.legalPlayer(field))
              {
                  auto newPlayer = Player(newPos, player.minute+1);
                  if (newPlayer !in alreadyLookingInto)
                  {
                      alreadyLookingInto[newPlayer] = true;
                      queue ~= newPlayer;
                  }
              }
          }
          return false;
      },
      (ref Player player)
      {
          // visited does not need to mark anything because its in theory ok to go back
      },
    );
    return minute;
}
@("example") unittest
{
    auto field = exampleData.parse;
    field.width.should == 8;
    field.height.should == 6;
    findExit(field, field.start, field.end).should == 18;
    findExit(field, field.end, field.start).should == 23;
    findExit(field, field.start, field.end).should == 13;
}

@("real") unittest
{
    import std.stdio : writeln;
    auto field = realData.parse;
    auto trip1 = field.findExit(field.start, field.end);
    writeln("part1: %s".format(trip1));
    auto trip2 = field.findExit(field.end, field.start);
    auto trip3 = field.findExit(field.start, field.end);
    writeln("part2: %s".format(trip1+trip2+trip3));
}

string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

auto eachCons(R)(R range, size_t length)
{
    struct EachCons
    {
        bool empty()
        {
            return range.length < length;
        }
        auto front()
        {
            return range[0..length];
        }
        void popFront()
        {
            range.popFront;
        }
    }
    return EachCons();
}

@("eachCons") unittest
{
    [1,2,3].eachCons(2).array.should == [[1, 2], [2, 3]];
    [1,2,3].eachCons(4).empty.should == true;
    [1,2,3].eachCons(4).array.empty.should == true;
}

void bfs(I)(void delegate(ref I[]) initial, bool delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    initial(queue);
    while (!queue.empty)
    {
        auto i = queue.front;
        queue.popFront;
        bool found = visit(i, queue);
        visited(i);
        if (found)
        {
            break;
        }
    }
}
