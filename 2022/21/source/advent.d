import std;
import optional;

class Database
{
    Monkey[string] name2Monkey;
    Monkey[][string] needs;
    auto resolve()
    {
        return name2Monkey["root"].calc(this);
    }
    auto maxVisited()
    {
        return reduce!((result, v) {
                return max(result, v.visited);
            })(0, name2Monkey.values);
    }
    auto secondPart()
    {
        auto root = name2Monkey["root"];
        root.op.op = "==";
        auto human = name2Monkey["humn"];
        human.number = none;
        root.calc(this);
        root.calcHuman(this);
        return name2Monkey["humn"].number.front;
    }
}

version (unittest)
{
    import unit_threaded;
}

auto exampleData()
{
    return `root: pppw + sjmn
dbpl: 5
cczh: sllz + lgvd
zczc: 2
ptdq: humn - dvpt
dvpt: 3
lfqf: 4
humn: 5
ljgn: 2
sjmn: drzm * dbpl
sllz: 4
pppw: cczh / lfqf
lgvd: ljgn * ptdq
drzm: hmdt - zczc
hmdt: 32`;
}

auto realData()
{
    return File("input.txt").readFully;
}

class N
{
    string name;
    long v;
    bool done;
}

class Operation
{
    string first;
    string second;
    string op;
    this(string s)
    {
        auto m = s.matchFirst("(....) (\\+|\\*|/|-) (....)");
        first = m[1];
        op = m[2];
        second = m[3];
    }
    auto calc(Database db)
    {
        auto m1 = db.name2Monkey[first].calc(db);
        auto m2 = db.name2Monkey[second].calc(db);
        if (m1.empty)
        {
            return m1;
        }
        if (m2.empty)
        {
            return m2;
        }
        switch (op)
        {
        case "+":
            return some(m1.front + m2.front);
        case "-":
            return some(m1.front - m2.front);
        case "/":
            return some(m1.front / m2.front);
        case "*":
            return some(m1.front * m2.front);
        default:
            throw new Exception("Unknown operation " ~ op);
        }
    }
}

class Monkey
{
    string name;
    Optional!long number;
    Operation op;
    int visited = 0;

    this(string s, Database database)
    {
        auto parts = s.split(":");
        this.name = parts[0].strip;
        database.name2Monkey[this.name] = this;
        auto m = parts[1].strip.matchFirst("(\\d+)");
        if (m)
        {
            number = some(m[1].to!long);
        }
        else
        {
            op = new Operation(parts[1].strip);
            database.needs[op.first] ~= this;
            database.needs[op.second] ~= this;
        }
    }
    auto calc(Database db)
    {
        visited++;
        if (op is null)
        {
            return number;
        }

        auto h = op.calc(db);
        if (h.empty)
        {
            return h;
        }
        op = null;
        number = h;
        return number;
    }

    auto calcHuman(Database db, long mustBe=0)
    {
        if (name == "humn")
        {
            number = some(mustBe);
            return;
        }

        if (name == "root")
        {
            auto left = db.name2Monkey[op.first];
            auto right = db.name2Monkey[op.second];
            if (left.number.empty)
            {
                return left.calcHuman(db, right.number.front);
            }
            return right.calcHuman(db, left.number.front);
        }

        if (op !is null)
        {
            auto left = db.name2Monkey[op.first];
            auto right = db.name2Monkey[op.second];
            if ((left.number.empty) && (right.number.empty))
            {
                throw new Exception("Both sides not ready");
            }

            if (left.number.empty)
            {
                final switch (op.op)
                {
                case "+":
                    // mustBe = left + right
                    return left.calcHuman(db, mustBe - right.number.front);
                case "-":
                    // mustBe = left - right
                    return left.calcHuman(db, mustBe + right.number.front);
                case "/":
                    // mustBe = left / right
                    return left.calcHuman(db, mustBe * right.number.front);
                case "*":
                    // mustBe = left * right
                    return left.calcHuman(db, mustBe / right.number.front);
                }
            }
            else if (right.number.empty)
            {
                final switch (op.op)
                {
                case "+":
                    // mustBe = left + right
                    return right.calcHuman(db, mustBe - left.number.front);
                case "-":
                    // mustBe = left - right
                    return right.calcHuman(db, left.number.front - mustBe);
                case "/":
                    // mustBe = left / right
                    return right.calcHuman(db, left.number.front / mustBe);
                case "*":
                    // mustBe = left * right
                    return right.calcHuman(db, mustBe / left.number.front);
                }
            }
        }
    }
}

auto parse(string s)
{
    auto result = new Database;
    s.strip.split("\n").map!(s => new Monkey(s, result)).array;
    return result;
}

@("example") unittest
{
    exampleData.parse.resolve.front.should == 152;
    exampleData.parse.secondPart.should == 301;
}

@("real") unittest
{
    writeln("part1: %s".format(realData.parse.resolve));
    writeln("part2: %s".format(realData.parse.secondPart));
}

string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

auto eachCons(R)(R range, size_t length)
{
    struct EachCons
    {
        bool empty()
        {
            return range.length < length;
        }
        auto front()
        {
            return range[0..length];
        }
        void popFront()
        {
            range.popFront;
        }
    }
    return EachCons();
}

@("eachCons") unittest
{
    [1,2,3].eachCons(2).array.should == [[1, 2], [2, 3]];
    [1,2,3].eachCons(4).empty.should == true;
    [1,2,3].eachCons(4).array.empty.should == true;
}
