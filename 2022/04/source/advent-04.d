module advent_04_01;

import std;


class Range
{
    int start;
    int end;
    this(int start, int end)
    {
        this.start = start;
        this.end = end;
    }
    bool includes(Range other)
    {
        return other.start >= this.start
            && other.start <= this.end
            && other.end >= this.start
            && other.end <= this.end;
    }
    bool overlaps(Range other)
    {
        return
            (this.start >= other.start && this.start <= other.end)
            || (this.end >= other.start && this.end <= other.end)
            || (other.start >= this.start && other.start <= this.end)
            || (other.end >= this.start && other.end <= this.end);
    }
}

@("Range includes") unittest
{
    import unit_threaded;
    new Range(10, 15).includes(new Range(10, 14)).should == true;
    new Range(10, 15).includes(new Range(10, 15)).should == true;
    new Range(10, 15).includes(new Range(10, 16)).should == false;
    new Range(10, 15).includes(new Range(9, 14)).should == false;
}

auto algo1(R)(R input)
{
    return input.count!(ab => ab[0].includes(ab[1]) || ab[1].includes(ab[0]));
}

auto algo2(R)(R input)
{
    return input.count!(ab => ab[0].overlaps(ab[1]));
}

@("Algo1") unittest
{
    import unit_threaded;
    [
      [
        new Range(1, 10),
        new Range(2, 8),
      ],
      [
        new Range(1,10),
        new Range(0,5),
      ],
    ].algo1.should == 1;
}

@("Algo2") unittest
{
    import unit_threaded;
    [
      [
        new Range(1, 10),
        new Range(2, 12),
      ],
      [
        new Range(1,10),
        new Range(0,5),
      ],
      [
        new Range(1,10),
        new Range(11, 12),
      ],
    ].algo2.should == 2;

    [
      "2-4,6-8",
      "2-3,4-5",
      "5-7,7-9",
      "2-8,3-7",
      "6-6,4-6",
      "2-6,4-8",
    ]
        .map!((line) {auto m = line.matchFirst(`(\d+)-(\d+),(\d+)-(\d+)`); return [new Range(m[1].to!int,m[2].to!int), new Range(m[3].to!int, m[4].to!int)];})
        .algo2
        .should == 4;
}

version (unittest)
{
}
else
{
    int main(string[] args)
    {
        if (args[1] == "1")
        {
            stdin
                .byLineCopy
                .map!((line) {auto m = line.matchFirst(`(\d+)-(\d+),(\d+)-(\d+)`); return [new Range(m[1].to!int,m[2].to!int), new Range(m[3].to!int, m[4].to!int)];})
                .algo1
                .writeln;
        } else if (args[1] == "2") {
            stdin
                .byLineCopy
                .map!((line) {auto m = line.matchFirst(`(\d+)-(\d+),(\d+)-(\d+)`); return [new Range(m[1].to!int,m[2].to!int), new Range(m[3].to!int, m[4].to!int)];})
                .algo2
                .writeln;
        }
        return 0;
    }
}
