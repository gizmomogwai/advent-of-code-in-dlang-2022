import std;

version (unittest)
{
    import unit_threaded;
}

auto exampleData()
{
    return `2,2,2
1,2,2
3,2,2
2,1,2
2,3,2
2,2,1
2,2,3
2,2,4
2,2,6
1,2,5
3,2,5
2,1,5
2,3,5`;
}

auto realData()
{
    return File("input.txt").readFully;
}

struct P
{
    int x;
    int y;
    int z;
    this(int[] d)
    {
        x = d[0];
        y = d[1];
        z = d[2];
    }
    this(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}

P parse(string s)
{
    return P(s.split(",").map!(s => s.to!int).array);
}

int countSurfaces(P[] input)
{
    bool[P] cubes;
    foreach (i; input)
    {
        cubes[i] = true;
    }
    int result = 0;
    foreach (i; input)
    {
        foreach (delta; [[-1, 0, 0], [1, 0, 0], [0, -1, 0], [0, 1, 0], [0, 0, -1], [0, 0, 1]])
        {
            if ((P(i.x+delta[0], i.y+delta[1], i.z+delta[2]) in cubes) is null)
            {
                result++;
            }
        }
    }
    return result;
}

void dfs(I)(void delegate(ref I[]) initial, void delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    queue.initial;
    while (!queue.empty)
    {
        auto i = queue.back;
        queue.popBack;
        i.visit(queue);
        i.visited;
    }
}
int waterReachableSurface(P[] cubes)
{
    auto extremes = cubes.reduce!(
      (a, b) => P(min(a.x, b.x), min(a.y, b.y), min(a.z, b.z)),
      (a, b) => P(max(a.x, b.x), max(a.y, b.y), max(a.z, b.z)),
    );
    auto minimum = extremes[0];
    auto maximum = extremes[1];
    minimum = P(minimum.x-1, minimum.y-1, minimum.z-1);
    maximum = P(maximum.x+1, maximum.y+1, maximum.z+1);
    bool[P] lava;
    bool[P] water;
    foreach (c; cubes)
    {
        lava[c] = true;
    }

    int count = 0;

    dfs!P(
      (ref P[] queue)
      {
          queue ~= minimum;
      },
      (ref P i, ref P[] queue)
      {
          if ((i in water) !is null)
          {
              return;
          }
          if ((i in lava) !is null)
          {
              count++;
              return;
          }

          foreach (delta; [[-1, 0, 0], [1, 0, 0], [0, -1, 0], [0, 1, 0], [0, 0, -1], [0, 0, 1]])
          {
              P newP = P(i.x+delta[0], i.y+delta[1], i.z+delta[2]);
              if (newP.x < minimum.x || newP.x > maximum.x || newP.y < minimum.y || newP.y > maximum.y || newP.z < minimum.z || newP.z > maximum.z)
              {
              }
              else
              {
                  queue ~= newP;
              }
          }

      },
      (ref P i)
      {
          water[i] = true;
      }
    );

    return count;
}

@("example") unittest
{
    auto data = exampleData.split("\n").map!parse.array;
    data.length.should == 13;
    data.countSurfaces.should == 64;
    data.waterReachableSurface.should == 58;
}

@("real") unittest
{
    import std.stdio : writeln;

    writeln("part1: %s".format(realData.split("\n").filter!(l => l.length > 0).map!parse.array.countSurfaces));

    writeln("part2: %s".format(realData.strip.split("\n").map!parse.array.waterReachableSurface));
}

string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

auto eachCons(R)(R range, size_t length)
{
    struct EachCons
    {
        bool empty()
        {
            return range.length < length;
        }
        auto front()
        {
            return range[0..length];
        }
        void popFront()
        {
            range.popFront;
        }
    }
    return EachCons();
}

@("eachCons") unittest
{
    [1,2,3].eachCons(2).array.should == [[1, 2], [2, 3]];
    [1,2,3].eachCons(4).empty.should == true;
    [1,2,3].eachCons(4).array.empty.should == true;
}
