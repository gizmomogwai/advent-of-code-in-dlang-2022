#!/usr/bin/env dub
/+
 dub.sdl:
 dependency "unit-threaded" version=">~2.1.2"
 +/
module advent_02_1;

import std;

enum Shape
{
    Rock,
    Paper,
    Scissors,
}
Shape string2Shape(string s)
{
    if ((s == "A") || (s == "X"))
        return Shape.Rock;
    if ((s == "B") || (s == "Y"))
        return Shape.Paper;
    if ((s == "C") || (s == "Z"))
        return Shape.Scissors;
    throw new Exception("Unknown shape " ~ s);
}

int shapeScore(Shape s)
{
    final switch (s)
    {
    case Shape.Rock:
        return 1;
    case Shape.Paper:
        return 2;
    case Shape.Scissors:
        return 3;
    }
}

int gameScore(Shape a, Shape b)
{
    final switch (a)
    {
    case Shape.Rock:
        final switch (b)
        {
        case Shape.Rock:
            return 3;
        case Shape.Paper:
            return 0;
        case Shape.Scissors:
            return 6;
        }
    case Shape.Paper:
        final switch (b)
        {
        case Shape.Rock:
            return 6;
        case Shape.Paper:
            return 3;
        case Shape.Scissors:
            return 0;
        }
    case Shape.Scissors:
        final switch (b)
        {
        case Shape.Rock:
            return 0;
        case Shape.Paper:
            return 6;
        case Shape.Scissors:
            return 3;
        }
    }
}

struct Round
{
    Shape other;
    Shape you;
    this(Shape[] ab)
    {
        other = ab[0];
        you = ab[1];
    }
    int score()
    {
        return you.shapeScore + gameScore(you, other);
    }
}
auto round(string[] strings)
{
    return Round(strings.map!(string2Shape).array);
}
auto parse(Lines)(Lines lines)
{
    Round[] result;
    foreach (line; lines)
    {
        result ~= round(line.idup.split(" "));
    }
    return result;
}
version (unittest)
{
}
else
{
    int main(string[] args)
    {
        auto input = parse(stdin.byLine);
        writeln(input.map!(i => i.score).sum);
        return 0;
    }
}

@("test example") unittest
{
    import unit_threaded;
    parse(["A Y", "B X", "C Z"]).map!(i => i.score).sum.should == 15;
}
