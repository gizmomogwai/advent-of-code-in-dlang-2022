#!/usr/bin/env dub
module advent_02_2;

import std;

enum Shape
{
    Rock,
    Paper,
    Scissors,
}
Shape string2Shape(string s)
{
    if ((s == "A") || (s == "X"))
        return Shape.Rock;
    if ((s == "B") || (s == "Y"))
        return Shape.Paper;
    if ((s == "C") || (s == "Z"))
        return Shape.Scissors;
    throw new Exception("Unknown shape " ~ s);
}

Result string2Result(string s)
{
    if (s == "X")
        return Result.Loss;
    if (s == "Y")
        return Result.Draw;
    if (s == "Z")
        return Result.Win;
    throw new Exception("Unknown result " ~ s);
}

int shapeScore(Shape s)
{
    final switch (s)
    {
    case Shape.Rock:
        return 1;
    case Shape.Paper:
        return 2;
    case Shape.Scissors:
        return 3;
    }
}

int gameScore(Shape a, Shape b)
{
    final switch (a)
    {
    case Shape.Rock:
        final switch (b)
        {
        case Shape.Rock:
            return 3;
        case Shape.Paper:
            return 0;
        case Shape.Scissors:
            return 6;
        }
    case Shape.Paper:
        final switch (b)
        {
        case Shape.Rock:
            return 6;
        case Shape.Paper:
            return 3;
        case Shape.Scissors:
            return 0;
        }
    case Shape.Scissors:
        final switch (b)
        {
        case Shape.Rock:
            return 0;
        case Shape.Paper:
            return 6;
        case Shape.Scissors:
            return 3;
        }
    }
}

enum Result
{
    Loss,
    Draw,
    Win,
}

struct Round
{
    Shape other;
    Result result;
    this(Shape other, Result result)
    {
        this.other = other;
        this.result = result;
    }
    int score()
    {
        auto om = ownMove;
        return om.gameScore(other) + om.shapeScore;
    }

    Shape ownMove()
    {
        final switch (other)
        {
        case Shape.Rock:
            final switch (result)
            {
            case Result.Loss:
                return Shape.Scissors;
            case Result.Draw:
                return Shape.Rock;
            case Result.Win:
                return Shape.Paper;
            }
        case Shape.Paper:
            final switch (result)
            {
            case Result.Loss:
                return Shape.Rock;
            case Result.Draw:
                return Shape.Paper;
            case Result.Win:
                return Shape.Scissors;
            }
        case Shape.Scissors:
            final switch (result)
            {
            case Result.Loss:
                return Shape.Paper;
            case Result.Draw:
                return Shape.Scissors;
            case Result.Win:
                return Shape.Rock;
            }
        }
    }
}
auto round(string[] strings)
{
    return Round(strings[0].string2Shape, strings[1].string2Result);
}
auto parse(Lines)(Lines lines)
{
    Round[] result;
    foreach (line; lines)
    {
        result ~= round(line.idup.split(" "));
    }
    return result;
}
version (unittest)
{
}
else
{
    int main(string[] args)
    {
        auto input = parse(stdin.byLine);
        writeln(input.map!(i => i.score).sum);
        return 0;
    }
}

@("test example") unittest
{
    import unit_threaded;
    parse(["A Y", "B X", "C Z"]).map!(i => i.score).sum.should == 12;
}
