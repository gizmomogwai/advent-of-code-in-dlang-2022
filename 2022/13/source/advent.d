import std;


string example()
{
return
`[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]`;
}

class Node {
    Node parent;
    this(Node parent)
    {
        this.parent = parent;
    }
}

class Int : Node {
    int v;
    this(Node parent, int v)
    {
        super(parent);
        this.v = v;
    }
    override string toString()
    {
        return v.to!string;
    }
}

class List : Node
{
    Node[] elements;
    this(Node parent)
    {
        super(parent);
    }
    auto add(Node n)
    {
        elements ~= n;
        return this;
    }
    override string toString()
    {
        return "[%s]".format(elements.map!(i => i.toString).join(","));
    }
}
class PacketData
{
    List data;
    this(List data)
    {
        this.data = data;
    }
    static PacketData parse(string s)
    {
        List result = null;
        List current = null;

        string currentNumber = "";
        foreach (c; s)
        {
            switch (c)
            {
            case '[':
                auto h = current;
                current = new List(current);
                if (result is null)
                {
                    result = current;
                }
                else
                {
                    h.add(current);
                }
                break;
            case ']':
                if (currentNumber.length > 0)
                {
                    current.add(new Int(current, currentNumber.to!int));
                    currentNumber = "";
                }
                current = cast(List)current.parent;
                break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                currentNumber ~= c;
                break;
            case ',':
                if (currentNumber.length > 0)
                {
                    current.add(new Int(current, currentNumber.to!int));
                    currentNumber = "";
                }
                break;
            default:
                throw new Exception("Unknown input: '" ~ c ~ "'");
            }
        }
        return new PacketData(result);
    }

    override string toString()
    {
        return "%s".format(data.toString);
    }
}

enum CMP {
    EQUAL,
    OK,
    NOK,
}

CMP rightOrder(Node[] left, Node[] right)
{
    if (left.empty)
    {
        if (right.empty)
        {
            return CMP.EQUAL;
        }
        else
        {
            return CMP.OK;
        }
    }
    else
    {
        if (right.empty)
        {
            return CMP.NOK;
        }
        else
        {
            auto c = rightOrder(left.front, right.front);
            if (c != CMP.EQUAL)
            {
                return c;
            }
            return rightOrder(left[1..$], right[1..$]);
        }
    }
}
CMP rightOrder(Node left, Node right)
{
    if (auto l = cast(Int)left)
    {
        if (auto r = cast(Int)right)
        {
            if (l.v == r.v)
            {
                return CMP.EQUAL;
            }
            else
            {
                return l.v < r.v ? CMP.OK : CMP.NOK;
            }
        }
        else
        {
            return rightOrder(new List(left.parent).add(left), right);
        }
    }
    else
    {
        auto l = cast(List)left;
        if (auto r = cast(List)right)
        {
            return rightOrder(l.elements, r.elements);
        }
        else
        {
            return rightOrder(l.elements, [right]);
        }
    }
}

CMP rightOrder(PacketData p1, PacketData p2)
{
    auto left = p1.data;
    auto right = p2.data;
    return rightOrder(left, right);
}

CMP rightOrder(PacketData[] packets)
{
    return rightOrder(packets[0], packets[1]);
}
auto parse(string input)
{
    return input.split("\n\n").map!((pair) {auto h = pair.split("\n"); return [PacketData.parse(h[0]), PacketData.parse(h[1])];}).array;
}
version (unittest)
{
    import unit_threaded;
}
@("e1") unittest
{
`[1,1,3,1,1]
[1,1,5,1,1]`.parse.front.rightOrder.should == CMP.OK;


`[[1],[2,3,4]]
[[1],4]`.parse.front.rightOrder.should == CMP.OK;

`[9]
[[8,7,6]]`.parse.front.rightOrder.should == CMP.NOK;

`[[4,4],4,4]
[[4,4],4,4,4]`.parse.front.rightOrder.should == CMP.OK;

`[7,7,7,7]
[7,7,7]`.parse.front.rightOrder.should == CMP.NOK;

`[]
[3]`.parse.front.rightOrder.should == CMP.OK;

`[[[]]]
[[]]`.parse.front.rightOrder.should == CMP.NOK;

`[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]`.parse.front.rightOrder.should == CMP.NOK;
}

int countRightOrder(T)(T pairs)
{
    return pairs.enumerate(1).filter!(t => t.value.rightOrder == CMP.OK).map!(t => t.index).sum;
}


@("example") unittest
{
    import unit_threaded;
    auto pairs = example.parse;

    pairs.length.should == 8;
    pairs.countRightOrder.should == 13;

    rightOrder(PacketData.parse("[[1],4]"), PacketData.parse("[1,[2,[3,[4,[5,6,7]]]],8,9]")).should == CMP.NOK;
    rightOrder(PacketData.parse("[1,[2,[3,[4,[5,6,7]]]],8,9]"), PacketData.parse("[[1],4]")).should == CMP.OK;

    auto p1 = PacketData.parse("[[2]]");
    auto p2 = PacketData.parse("[[6]]");
    auto sorted = (pairs.join ~ p1 ~ p2).sort!((a, b) {
            return rightOrder(a, b) == CMP.OK;
        });
    auto i1 = sorted.countUntil(p1)+1;
    auto i2 = sorted.countUntil(p2)+1;
    (i1 * i2).should == 140;
}

@("real data") unittest
{
    import std.stdio : writeln;
    auto pairs = File("input.txt").readFully.parse;
    writeln("part1: ", pairs.countRightOrder);

    auto p1 = PacketData.parse("[[2]]");
    auto p2 = PacketData.parse("[[6]]");
    auto sorted = (pairs.join ~ p1 ~ p2).sort!((a, b) {
            return rightOrder(a, b) == CMP.OK;
        });
    auto i1 = sorted.countUntil(p1)+1;
    auto i2 = sorted.countUntil(p2)+1;
    writeln("part2: ", i1 * i2);
}

string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

version (unittest)
{
}
else
{
    int main(string[] args)
    {
        auto data = stdin
            .readFully
            .parse;

        return 0;
    }
}
