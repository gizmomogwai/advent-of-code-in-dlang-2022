import std; 

class VM
{
    int x = 1;
    string[] input;
    ulong instruction;

    abstract class Command
    {
        abstract bool cycle(VM vm);
    }
    class Noop : Command
    {
        override bool cycle(VM vm)
        {
            return true;
        }
    }
    class Add : Command
    {
        int v;
        int cycles = 2;
        this(int v)
        {
            this.v = v;
        }
        override bool cycle(VM vm)
        {
            cycles--;
            if (cycles == 0)
            {
                vm.x += v;
                return true;
            }
            return false;
        }
    }
    this(string[] input)
    {
        this.input = input;
    }
    Command currentCommand;
    Command parse(string s)
    {
        if (s == "noop")
        {
            return new Noop;
        }
        else
        {
            auto m = s.matchFirst("addx (.+)");
            return new Add(m[1].to!int);
        }
    }

    void cycle(int cycles)
    {
        for (int i=0; i<cycles; i++)
        {
            if (currentCommand is null)
            {
                currentCommand = parse(input[instruction++]);
            }
            if (currentCommand.cycle(this))
            {
                currentCommand = null;
            }
        }
    }
}
@("example") unittest
{
    import unit_threaded;

    auto input = [
      "noop",
      "addx 3",
      "addx -5",
    ];

    VM vm = new VM(input);
    vm.x.should == 1;
    vm.cycle(1);
    vm.x.should == 1; // after 1st

    vm.cycle(2);
    vm.x.should == 4;
    vm.cycle(2);
    vm.x.should == -1;
}

int algo(string[] data)
{
    int sum = 0;
    foreach (i; [19, 59, 99, 139, 179, 219])
    {
        auto vm = new VM(data);
        vm.cycle(i);
        writeln(vm.x, " * ", i+1);
        sum += (i+1) * vm.x;
    }
    return sum;
}

auto example()
{
    return
        [
      "addx 15",
      "addx -11",
      "addx 6",
      "addx -3",
      "addx 5",
      "addx -1",
      "addx -8",
      "addx 13",
      "addx 4",
      "noop",
      "addx -1",
      "addx 5",
      "addx -1",
      "addx 5",
      "addx -1",
      "addx 5",
      "addx -1",
      "addx 5",
      "addx -1",
      "addx -35",
      "addx 1",
      "addx 24",
      "addx -19",
      "addx 1",
      "addx 16",
      "addx -11",
      "noop",
      "noop",
      "addx 21",
      "addx -15",
      "noop",
      "noop",
      "addx -3",
      "addx 9",
      "addx 1",
      "addx -3",
      "addx 8",
      "addx 1",
      "addx 5",
      "noop",
      "noop",
      "noop",
      "noop",
      "noop",
      "addx -36",
      "noop",
      "addx 1",
      "addx 7",
      "noop",
      "noop",
      "noop",
      "addx 2",
      "addx 6",
      "noop",
      "noop",
      "noop",
      "noop",
      "noop",
      "addx 1",
      "noop",
      "noop",
      "addx 7",
      "addx 1",
      "noop",
      "addx -13",
      "addx 13",
      "addx 7",
      "noop",
      "addx 1",
      "addx -33",
      "noop",
      "noop",
      "noop",
      "addx 2",
      "noop",
      "noop",
      "noop",
      "addx 8",
      "noop",
      "addx -1",
      "addx 2",
      "addx 1",
      "noop",
      "addx 17",
      "addx -9",
      "addx 1",
      "addx 1",
      "addx -3",
      "addx 11",
      "noop",
      "noop",
      "addx 1",
      "noop",
      "addx 1",
      "noop",
      "noop",
      "addx -13",
      "addx -19",
      "addx 1",
      "addx 3",
      "addx 26",
      "addx -30",
      "addx 12",
      "addx -1",
      "addx 3",
      "addx 1",
      "noop",
      "noop",
      "noop",
      "addx -9",
      "addx 18",
      "addx 1",
      "addx 2",
      "noop",
      "noop",
      "addx 9",
      "noop",
      "noop",
      "noop",
      "addx -1",
      "addx 2",
      "addx -37",
      "addx 1",
      "addx 3",
      "noop",
      "addx 15",
      "addx -21",
      "addx 22",
      "addx -6",
      "addx 1",
      "noop",
      "addx 2",
      "addx 1",
      "noop",
      "addx -10",
      "noop",
      "noop",
      "addx 20",
      "addx 1",
      "addx 2",
      "addx 2",
      "addx -6",
      "addx -11",
      "noop",
      "noop",
      "noop",
        ];
}

@("first example") unittest
{
    import unit_threaded;
    example.algo.should == 13140;
}


string crt(string[] data)
{
    string crt;
    int line = 0;
    VM vm = new VM(data);
    for (int j=0; j<6; ++j){
        for (int cycle=0; cycle<40; cycle++)
        {
            writeln("line ", j, " cycle: ", cycle, ": ", vm.x);
            if ((cycle-vm.x).abs <= 1)
            {
                crt ~= "#";
            }
            else
            {
                crt ~= ".";
            }
            vm.cycle(1);
        }
        crt~="\n";
    }
    return crt;
}

@("crt example") unittest
{
    example.crt.writeln;
}

version (unittest)
{
}
else
{
    int main(string[] args)
    {
        auto data = stdin
            .byLineCopy
            .array;
        {
            data.algo.writeln;
        }

        {
            data.crt.writeln;
        }
        return 0;
    }
}
