import std;

string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

class F
{
    string name;
    int size;
    bool dir;
    F parent;
    F[string] childs;
    this(string name, int size)
    {
        this.name = name;
        this.size = size;
        this.dir = size == 0;
    }
    void add(F f)
    {
        childs[f.name] = f;
        f.parent = this;
    }
    string print(int ident)
    {
        auto res = "%s%s (%s)".format(' '.repeat(ident), name, calcSize);
        foreach (name, file; childs)
        {
            res ~= "\n" ~ file.print(ident+2);
        }
        return res;
    }
    int calcSize()
    {
        if (size == 0)
        {
            size = childs.values.map!(f => f.calcSize).sum;
        }
        return size;
    }
}

class FS
{
    F cwd;
    F root;

    void cd(string s)
    {
//        writeln("cd '", s, "'");
        if (s == "..")
        {
            cwd = cwd.parent;
        }
        else if (s == "/")
        {
            if (root is null)
            {
                root = new F("/", 0);
            }
            cwd = root;
        }
        else
        {
            cwd = cwd.childs[s];
        }
    }
    void add(string file)
    {
//        writeln("add '", file, "'");
        auto h = file.split(" ");
        auto size = h[0];
        auto name = h[1];
        if (size == "dir")
        {
            cwd.add(new F(name, 0));
        }
        else
        {
            cwd.add(new F(name, size.to!int));
        }
    }
    override string toString()
    {
        return root.print(0);
    }
}

auto parse(string[] lines)
{
    auto l = lines;
    auto fs = new FS;
    while (!l.empty)
    {
        auto h = l.front; l.popFront;
        if (h.startsWith("$ cd"))
        {
            fs.cd(h[5..$]);
        }
        else if (h.startsWith("$ ls"))
        {
            auto line = l.front;
            while (!line.startsWith("$"))
            {
                fs.add(line);
                l.popFront;
                if (l.empty)
                {
                    writeln("done");
                    break;
                }
                line = l.front;
                if (line == "")
                {
                    break;
                }
            }
        }
    }
    return fs;
}

void walk(F f, void delegate(F) fun)
{
    fun(f);
    foreach (file; f.childs)
    {
        walk(file, fun);
    }
}

int main(string[] args)
{
    auto fs = File("input.txt")
        .readFully
        .split("\n")
        .array
        .parse;
    fs.writeln;
    int sum = 0;
    void sumOfDirsAtMost100000(F f)
    {
        if (f.dir && f.size <= 100000)
        {
            sum += f.size;
        }
    }
    walk(fs.root, &sumOfDirsAtMost100000);
    writeln(sum);

    int total = 70000000;
    int needed = 30000000;
    int used = fs.root.size;
    int unused = total - used;
    int minSize = needed - unused;
    int dirToDelete = total;
    void dirToDeleteF(F f)
    {
        if (f.dir && f.size >= minSize)
        {
            dirToDelete = min(f.size, dirToDelete);
        }
    }
    walk(fs.root, &dirToDeleteF);

    writeln("total:       ", total);
    writeln("needed:      ", needed);
    writeln("used:        ", used);
    writeln("unused:      ", unused);
    writeln("minrequired: ", minSize);
    writeln("good dir:    ", dirToDelete);
    return 0;
}
