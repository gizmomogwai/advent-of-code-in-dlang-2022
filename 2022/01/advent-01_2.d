#!/usr/bin/env rdmd
module advent01_2;
import std;
class Elv
{
    int[] proviant;

    void add(int p)
    {
        proviant ~= p;
    }
    int total()
    {
        return proviant.sum;
    }
    override string toString()
    {
        return "Elve: " ~ proviant.to!string ~ ": " ~ total.to!string;
    }
}

auto parse()
{
    return stdin.byLine.fold!(
      (result, line)
      {
          if (line == "")
          {
              result ~= new Elv;
          }
          else
          {
              result[$-1].add(line.to!int);
          }
          return result;
      })([new Elv]);
}

int main(string[] args)
{
    parse
        .map!(i => i.total).array
        .sort
        .tail(3)
        .sum
        .writeln
        ;
    return 0;
}
