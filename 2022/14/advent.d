import std;
version (unittest)
{
    import unit_threaded;
}

auto exampleData()
{
    return "TODO";
}

auto realData()
{
    return "input.txt".readFully;
}

@("example") unittest
{
    true.should == false;
}

@("real") unittest
{
    import std.stdio : writeln;

    writeln("part1: %s".format("TODO"));

    writeln("part2: %s".format("TODO"));
}

version (unittest)
{
}
else
{
    int main(string[] args)
    {
        auto data = stdin
            .byLineCopy
            .array;

        return 0;
    }
}
