import std;

version (unittest)
{
    import unit_threaded;
}

string example()
{
    return
        `498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9`;
}

auto eachCons(R)(R range, size_t length)
{
    struct EachCons
    {
        bool empty()
        {
            return range.length < length;
        }
        auto front()
        {
            return range[0..length];
        }
        void popFront()
        {
            range.popFront;
        }
    }
    return EachCons();
}

@("eachCons") unittest
{
    import std.stdio;
    [1,2,3].eachCons(2).array.should == [[1, 2], [2, 3]];
    [1,2,3].eachCons(4).empty.should == true;
    [1,2,3].eachCons(4).array.empty.should == true;
}

struct P
{
    int x;
    int y;
    this(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    static auto parse(string s)
    {
        auto coords = s.split(",").map!(s => s.to!int).array;
        return P(coords[0], coords[1]);
    }
    string toString()
    {
        return "P(%s,%s)".format(x, y);
    }
}
class Cave
{
    char[P] data;
    P topLeft;
    P bottomRight;
    this(string rockSegments)
    {
        foreach (segment; rockSegments.split("\n"))
        {
            auto points = segment.split(" -> ").map!(P.parse);
            foreach (pair; points.eachCons(2))
            {
                drawRocks(pair[0], pair[1]);
            }
        }
        auto extremes = data.keys.reduce!(
          (a, b) => P(min(a.x, b.x), min(a.y, b.y)),
          (a, b) => P(max(a.x, b.x), max(a.y, b.y)),
        );
        topLeft = extremes[0];
        bottomRight = extremes[1];
    }

    private void drawRocks(P from, P to)
    {
        auto dx = (to.x - from.x).sgn;
        auto dy = (to.y - from.y).sgn;
        data[from] = '#';
        while (from != to)
        {
            from.x += dx;
            from.y += dy;
            data[from] = '#';
        }
    }

    bool abyss(P p, bool floor)
    {
        if (floor)
        {
            return false;
        }
        if (p.y > bottomRight.y)
        {
            return true;
        }
        return false;
    }

    auto sand(bool floor = false)
    {
        auto sand = P(500, 0);
        while (true)
        {
            auto oldSand = sand;
            foreach (next; [P(sand.x, sand.y+1),
                            P(sand.x-1, sand.y+1),
                            P(sand.x+1,sand.y+1)])
            {
                if (abyss(next, floor))
                {
                    return false;
                }
                if (air(next))
                {
                    sand = next;
                    break;
                }
            }

            if (oldSand == sand) // sand could not move anymore
            {
                data[sand] = 'o';
                if (sand == P(500, 0))
                {
                    return false;
                }
                return true;
            }
        }
    }

    /// checks if cave is already occupied at p
    bool air(P p)
    {
        if (p.y == bottomRight.y+2)
        {
            return false;
        }
        return (p in data) is null;
    }

    int countSand()
    {
        int i=0;
        while (sand)
        {
            i++;
            //       writeln("----------------------------------------");
            // "%s".format(this).writeln;
        }
        return i;
    }

    int countSandTillSafe()
    {
        int i=0;
        while (sand(true))
        {
            i++;
        }
        return i+1;
    }

    override string toString()
    {
        string res;
        for (int j=min(0, topLeft.y); j<=max(0, bottomRight.y); ++j)
        {
            for (int i=min(500, topLeft.x); i<=max(500, bottomRight.x); ++i)
            {
                auto h = P(i, j);
                if (h in data)
                {
                    res ~= '#';
                }
                else
                {
                    res ~= '.';
                }
            }
            res ~= "\n";
        }
        return res;
    }
}

@("example") unittest
{
    auto cave = new Cave(example);
    cave.countSand.should == 24;

    cave = new Cave(example);
    cave.countSandTillSafe.should == 93;
}

@("real data") unittest
{
    auto input = File("input.txt").readFully;
    auto cave = new Cave(input);
    writeln("part1: ", cave.countSand);
    cave = new Cave(input);
    writeln("part2: ", cave.countSandTillSafe);
}

string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}
