import std;

version (unittest)
{
    import unit_threaded;
}

auto exampleData()
{
    return `1
2
-3
3
-2
0
4`;
}

auto realData()
{
    return File("input.txt").readFully.strip;
}

class Number
{
    long v;
    this(string  s)
    {
        this.v = s.to!int;
    }
    this(long v)
    {
        this.v = v;
    }
    override string toString()
    {
        return v.to!string;
    }
}
auto parse(string s)
{
    return s.split("\n").map!(s => new Number(s)).array;
}

void move(ref Number[] numbers, size_t index, Number n)
{
    // TODO
}

pure T mod(T)(T n, T d) if (isIntegral!(T))
{
    T r = n % d;
    return sgn(r) == -(sgn(d)) ? r + d : r;
}

@("numbers with same value") unittest
{
    auto n1 = new Number("1");
    auto n2 = new Number("1");
    [n1, n2].countUntil(n1).should == 0;
    [n1, n2].countUntil(n2).should == 1;
}

auto mix(Number[] numbers, int count=1)
{
    auto original = numbers;
    auto work = original.dup;
    auto length = work.length.to!int;

    foreach (i; count.iota)
    {
        foreach (n; original)
        {
            auto idx = work.countUntil(n).to!int;
            if (n.v != 0)
            {
                work.remove(idx);
                auto h = mod(idx + n.v, length-1);
                work.insertInPlace(h, n);
                work.length = length;
            }
        }
    }
    return work;
}

auto calc(Number[] numbers)
{
    auto offset = numbers.countUntil!("a.v == 0");
    return numbers[(offset + 1000) % numbers.length].v
        + numbers[(offset + 2000) % numbers.length].v
        + numbers[(offset+ 3000) % numbers.length].v;
}
@("example") unittest
{
    exampleData.parse.mix.calc.should == 3;
    exampleData.parse.map!(i => new Number(i.v*811589153)).array.mix(10).calc.should == 1623178306;
}

@("real") unittest
{
    writeln("part1: %s".format(realData.parse.mix.calc));

    writeln("part2: %s".format(realData.parse.map!(i => new Number(i.v*811589153)).array.mix(10).calc));
}

string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

auto eachCons(R)(R range, size_t length)
{
    struct EachCons
    {
        bool empty()
        {
            return range.length < length;
        }
        auto front()
        {
            return range[0..length];
        }
        void popFront()
        {
            range.popFront;
        }
    }
    return EachCons();
}

@("eachCons") unittest
{
    [1,2,3].eachCons(2).array.should == [[1, 2], [2, 3]];
    [1,2,3].eachCons(4).empty.should == true;
    [1,2,3].eachCons(4).array.empty.should == true;
}

version (unittest)
{
}
else
{
    int main(string[] args)
    {
        writeln("part1: %s".format(realData.parse.mix.calc));
        writeln("part2: %s".format(realData.parse.map!(i => new Number(i.v*811589153)).array.mix(10).calc));
        return 0;
    }
}
