module advent_03_02;
import std;

int prio(char c)
{
    if (c >= 'a' && c <= 'z')
    {
        return c-'a'+1;
    }

    if (c >= 'A' && c <= 'Z')
    {
        return c-'A'+27;
    }
    throw new Exception("unknown char " ~ c);
}

char findBadge(Range)(Range r)
{
    auto a = r.front;
    r.popFront;
    auto b = r.front;
    r.popFront;
    auto c = r.front;

    foreach (ch; a)
    {
        if (!(b.find(ch) == "") && !(c.find(ch) == ""))
        {
            return ch;
        }
    }
    throw new Exception("Cannot find badge for " ~ a ~ ", " ~ b ~ ", " ~ c);
}
int algo(Range)(Range range)
{
    return range
        .chunks(3)
        .map!(findBadge)
        .map!(prio)
        .sum;
}

@("example") unittest
{
    import unit_threaded;
    [
      "vJrwpWtwJgWrhcsFMMfFFhFp",
      "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
      "PmmdzqPrVvPwwTWBwg",
      "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
      "ttgJtRGJQctTZtZT",
      "CrZsJsPPZsGzwwsLwLmpwMDw",
    ].algo.should == 70;
}

version (unittest)
{
}
else
{
    int main(string[] args)
    {
        stdin
            .byLineCopy(No.keepTerminator)
            .algo
            .writeln;
        return 0;
    }
}
