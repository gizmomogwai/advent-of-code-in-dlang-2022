module advent_03_01;
import std;

char findDoubleItem(string s1, string s2)
{
    foreach (c; s1)
    {
        if (!s2.find(c).empty)
        {
            return c;
        }
    }
    throw new Exception("there should be a double in " ~s1 ~ " and " ~ s2);
}

int prio(char c)
{
    if (c >= 'a' && c <= 'z')
    {
        return c-'a'+1;
    }

    if (c >= 'A' && c <= 'Z')
    {
        return c-'A'+27;
    }
    throw new Exception("unknown char " ~ c);
}
@("findDoubleItem") unittest
{
    import unit_threaded;
    findDoubleItem("abc", "bde").should == 'b';
}

@("example") unittest
{
    import unit_threaded;
    [
      "vJrwpWtwJgWrhcsFMMfFFhFp",
      "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
      "PmmdzqPrVvPwwTWBwg",
      "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
      "ttgJtRGJQctTZtZT",
      "CrZsJsPPZsGzwwsLwLmpwMDw",
    ].algo.should == 157;
}

int algo(Range)(Range range)
{
    return range
        .map!(s => [s[0..s.length/2], s[s.length/2..$]])
        .map!(ab => findDoubleItem(ab[0], ab[1]))
        .map!(c => c.prio)
        .sum;
}

version (unittest)
{
}
else
{
int main(string[] args)
{
    stdin
        .byLineCopy(No.keepTerminator)
        .algo
        .writeln;
    return 0;
}
}
