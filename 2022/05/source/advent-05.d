module advent_05;
import std;
//import tango.util.container.more : Stack;

alias Stack = immutable(char)[];


class Move
{
    int count;
    int from;
    int to;
    this(string l)
    {
        auto m = l.matchFirst(regex(`move (\d+) from (\d+) to (\d+)`));
        count = m[1].to!int;
        from = m[2].to!int -1;
        to = m[3].to!int -1;
    }
    override string toString()
    {
        return "Move %s from %s to %s".format(count, from, to);
    }
}

void addLine(Stack[] result, string line)
{
    for (int i=0; i<result.length; ++i)
    {
        if (i * 4 +1 < line.length)
        {
            auto h = line[i*4+1];
            if (h != ' ')
            {
                result[i] ~= line[i*4+1];
            }
        }
    }
}

Stack[] parseStacks(string[] l)
{
    Stack[] result;
    auto lines = l.reverse;
    auto nrOfStacks = (lines.front.length+1) / 4; lines.popFront;
    result = new Stack[nrOfStacks];
    foreach (line; lines)
    {
        result.addLine(line);
    }
    writeln(result);
    return result;
}

Move[] parseMoves(string[] moves)
{
    return moves.map!(l => new Move(l)).array;
}

Stack[] apply(Stack[] stacks, Move[] moves)
{
    foreach (move; moves)
    {
        for (int i=0; i<move.count; ++i)
        {
            auto h = stacks[move.from].back;
            stacks[move.from].popBack;
            stacks[move.to] ~= h;
        }
    }
    return stacks;
}
Stack[] apply2(Stack[] stacks, Move[] moves)
{
    foreach (move; moves)
    {
        Stack help;
        for (int i=0; i<move.count; ++i)
        {
            auto h = stacks[move.from].back;
            stacks[move.from].popBack;
            help ~= h;
        }
        for (int i=0; i<move.count; ++i)
        {
            stacks[move.to] ~= help.back;
            help.popBack;
        }
    }
    return stacks;
}

string algo(Input)(Input input)
{
    auto data = input.split("\n\n");
    auto stacks = data.front.split("\n").array.parseStacks; data.popFront;
    auto moves = data.front.split("\n").array.parseMoves;
    auto newStacks = stacks.apply(moves);
    return newStacks.map!(s => s.back).to!string;
}

string algo2(Input)(Input input)
{
    auto data = input.split("\n\n");
    auto stacks = data.front.split("\n").array.parseStacks; data.popFront;
    auto moves = data.front.split("\n").array.parseMoves;
    auto newStacks = stacks.apply2(moves);
    return newStacks.map!(s => s.back).to!string;
}

version (unittest)
{
}
else
{
    int main(string[] args)
    {
        if (args[1] == "1")
        {
            stdin
                .byLineCopy
                .join("\n")
                .algo
                .writeln;
        }
        else if (args[1] == "2")
        {
            stdin
                .byLineCopy
                .join("\n")
                .algo2
                .writeln;
        }
        return 0;
    }

}
