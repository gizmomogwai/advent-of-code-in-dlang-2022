import std;

struct Position
{
    int x;
    int y;
    this(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
}

auto distanceTo(Position p1, Position p2)
{
    return max((p1.x - p2.x).abs, (p1.y - p2.y).abs);
}

auto moveTo(Position tail, Position head)
{
    if (tail.distanceTo(head) > 1)
    {
        int dx = (head.x - tail.x).sgn;
        int dy = (head.y - tail.y).sgn;
        return Position(tail.x + dx, tail.y + dy);
    }
    return tail;
}

class Field
{
    bool[Position] visited;

    auto countVisited()
    {
        return visited.length;
    }
    auto interprete(string[] moves, int tailLength)
    {
        Position[] snake = new Position[tailLength];
        visited[snake[$-1]] = true;
        foreach (m; moves)
        {
            auto move = m.split(" ");
            auto direction = move[0];
            auto distance = move[1].to!int;

            for (int i=0; i<distance; ++i)
            {
                int dx = 0;
                int dy = 0;
                switch (direction)
                {
                case "R":
                    dx = 1;
                    break;
                case "L":
                    dx = -1;
                    break;
                case "U":
                    dy = 1;
                    break;
                case "D":
                    dy = -1;
                    break;
                default:
                    throw new Exception("unknown direction " ~ direction);
                }

                snake[0] = Position(snake[0].x + dx, snake[0].y + dy);
                for (int j=1; j<snake.length; ++j)
                {
                    auto newOne = snake[j].moveTo(snake[j-1]);
                    snake[j] = newOne;
                }

                visited[snake[$-1]] = true;
            }
        }
        return this;
    }
}

@("example") unittest
{
    import unit_threaded;
    bool[Position] t;
    t[Position(0, 0)] = true;
    t[Position(0, 0)] = true;
    t.length.should == 1;


    new Field().interprete([
                             "R 4",
                             "U 4",
                             "L 3",
                             "D 1",
                             "R 4",
                             "D 1",
                             "L 5",
                             "R 2",
                           ], 2).countVisited.should == 13;

}

version (unittest)
{
}
else
{
    int main(string[] args)
    {
        auto moves = stdin
            .byLineCopy
            .array;

        auto field = new Field;
        field.interprete(moves, 2);
        field.countVisited.writeln;

        field = new Field;
        field.interprete(moves, 10);
        field.countVisited.writeln;

        return 0;
    }
}
