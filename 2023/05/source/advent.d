import std;

version (unittest)
{
    import unit_threaded;
}

auto exampleInput()
{
    return `seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4
`;
}

auto realInput()
{
    return File("input.txt").readFully;
}

struct Mapping
{
    long from;
    long length;
    long to;
    long delta()
    {
        return to-from;
    }
    bool includes(long v)
    {
        return v >= from && v < from + length;
    }
    long map(long v)
    {
        return v+delta();
    }
}

class Mappings
{
    string from;
    string to;
    Mapping[] mappings;
    this(string from, string to)
    {
        this.from = from;
        this.to = to;
    }
    override string toString()
    {
        return "from: %s to: %s\n%s".format(from, to, mappings.to!string);
    }
    long map(long source)
    {
        foreach (m; mappings)
        {
            if (m.includes(source)) return m.map(source);
        }
        return source;
    }
}

class Almanac
{
    long[] seeds;
    Mappings[string] mappings; // map from from to a mapping
    override string toString()
    {
        return "seeds: " ~ seeds.map!(i => i.to!string).join(", ").to!string ~ "\nmappings: " ~ mappings.to!string;
    }
    long follow(long v, string from="seed")
    {
        if (from == "location") return v;
        auto m = mappings[from];
        return follow(m.map(v), m.to);
    }
    long calcNumberOfSeeds()
    {
        return seeds.chunks(2).map!(i => i[1]).sum;
    }
}


auto parse(string input)
{
    Almanac result = new Almanac;
    auto lines = input.split("\n");
    string from;
    string to;
    Mappings m;
    foreach (line; lines)
    {
        if (line.empty) continue;
        if (result.seeds.empty)
        {
            result.seeds = line.split(": ")[1].split(" ").map!(s => s.to!long).array;
        }
        else
        {
            auto match = line.matchFirst(regex(`(.+)-to-(.+) map\:`));
            if (match)
            {
                m = new Mappings(match[1], match[2]);
                result.mappings[m.from] = m;
            }
            else
            {
                auto parts = line.split(" ");
                m.mappings ~= Mapping(parts[1].to!long, parts[2].to!long, parts[0].to!long);
            }
        }
    }
    return result;
}

auto part1(T)(T input)
{
    return input.seeds.map!(seed => input.follow(seed)).minElement;
}

@("part1") unittest
{
    auto almanac = exampleInput.parse;
    auto seedToSoil = almanac.mappings["seed"];
    seedToSoil.from.should == "seed";
    seedToSoil.to.should == "soil";
    seedToSoil.map(98).should == 50;
    seedToSoil.map(99).should == 51;
    seedToSoil.map(100).should == 100;
    seedToSoil.map(50).should == 52;
    seedToSoil.map(97).should == 99;

    almanac.follow(79).should == 82;
    almanac.follow(14).should == 43;
    almanac.follow(55).should == 86;
    almanac.follow(13).should == 35;

    almanac.part1.should == 35;
}

long findMin(T)(Almanac a, T range)
{
    long result = a.follow(range[0]);
    int oldPercent = 0;
    for (int i=1; i<range[1]; ++i) {
        if (i % 1000 == 0) {
            float percent = (i.to!float*100)/range[1];
            if (percent.to!int != oldPercent) {
                oldPercent = percent.to!int;
                writeln("percent: ", oldPercent);
            }
        }
        result = min(result, a.follow(range[0] + i));
    }
    writeln("one seedrange done");
    return result;
}
auto part2(Almanac input)
{
    /+
    auto h = input.seeds.chunks(2).array;
    return taskPool.amap!(range => findMin(input, range))(h).minElement;
    +/
    long[] result = new long[input.seeds.chunks(2).length];
    foreach (idx, range; input.seeds.chunks(2).parallel)
    {
        result[idx] = input.findMin(range);
    }
    return result.minElement;
}

@("part2") unittest
{
    auto almanac = exampleInput.parse;
    almanac.part2.should == 46;
    writeln(almanac.calcNumberOfSeeds());
}

@("real") unittest
{
    import std.stdio : writeln;

    writeln("part1: %s".format(realInput.parse.part1));
    writeln("part2: %s".format(realInput.parse.part2));
}

string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

auto eachCons(R)(R range, size_t length)
{
    struct EachCons
    {
        bool empty()
        {
            return range.length < length;
        }
        auto front()
        {
            return range[0..length];
        }
        void popFront()
        {
            range.popFront;
        }
    }
    return EachCons();
}

@("eachCons") unittest
{
    [1,2,3].eachCons(2).array.should == [[1, 2], [2, 3]];
    [1,2,3].eachCons(4).empty.should == true;
    [1,2,3].eachCons(4).array.empty.should == true;
}

void dfs(I)(void delegate(ref I[]) initial, void delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    queue.initial;
    while (!queue.empty)
    {
        auto i = queue.back;
        queue.popBack;
        i.visit(queue);
        i.visited;
    }
}

void bfs(I)(void delegate(ref I[]) initial, bool delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    initial(queue);
    while (!queue.empty)
    {
        auto i = queue.front;
        queue.popFront;
        bool found = visit(i, queue);
        visited(i);
        if (found)
        {
            break;
        }
    }
}
