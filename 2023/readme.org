* Links
- https://github.com/schveiguy/adventofcode/tree/master/2023 d solutions
- https://github.com/andrewlalis/AdventOfCode2023/tree/main d solutions

* Days
** 01
- part1 ok .. besides empty lines
- part2 annoying as the numbers are overlapping so a repalce did not work

** 02
- parsing takes too long for me
- coming up with good names for drawsets/draws/... is hard for me

** 03
- problem with a number ending at the end of lines took me quite too
  long
- part2 was quicker

** 04
- again too long with the parsing .. especially leading spaces and
  double spaces throw off split(" "), better to
  trim.split(regex(`\s+`)) ...

** 05
- first one was ok,
- brute forcing the 2nd one was time consuming :/

** 06
- brute forcing is not a problem in this case

* Comparisons with rust
- rust compiles faster than ldc by factor 4 for my programs
- rust filter_map seems to be nice ... but is it really needed? filter
  in dlang followed by map does not need to unwrap, so perhaps thats
  the not needed
