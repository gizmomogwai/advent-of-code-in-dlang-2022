import std;

version (unittest)
{
    import unit_threaded;
}

auto exampleInput()
{
    return `Time:      7  15   30
Distance:  9  40  200
`;
}

auto realInput()
{
    return File("input.txt").readFully;
}

struct Race
{
    long duration;
    long oldRecord;
    long betterResults;
    long[] calcResults()
    {
        return iota(1, duration-1).map!(chargeDuration => (duration-chargeDuration)*chargeDuration).array;
    }
}
auto parse(string input)
{
    auto lines =input.split("\n");
    auto times = lines[0].split()[1..$];
    auto oldRecords = lines[1].split()[1..$];
    return times.zip(oldRecords).map!(pair => Race(pair[0].to!int, pair[1].to!int)).array;
}

auto parse2(string input)
{
    string h = "";
    foreach (c; input)
    {
        if (c != ' ')
            h ~= c;
    }
    auto lines = h.split("\n");
    auto duration = lines[0].split(":")[1].to!long;
    auto oldRecord = lines[1].split(":")[1].to!long;
    return Race(duration, oldRecord);
}

auto part1(T)(T input)
{
    foreach (ref race; input)
    {
        race.betterResults = race.calcResults.filter!(result => race.oldRecord < result).count;
    }
    return input.fold!((result, v) => result * v.betterResults)(1L);
}

@("part1") unittest
{
    exampleInput.parse.part1.should == 288;
}

auto part2(T)(T input)
{
    return [input].part1;
}

@("part2") unittest
{
    exampleInput.parse2.part2.should == 71503;
}

@("real") unittest
{
    import std.stdio : writeln;

    writeln("part1: %s".format(realInput.parse.part1));

    writeln("part2: %s".format(realInput.parse2.part2));
}

string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

auto eachCons(R)(R range, size_t length)
{
    struct EachCons
    {
        bool empty()
        {
            return range.length < length;
        }
        auto front()
        {
            return range[0..length];
        }
        void popFront()
        {
            range.popFront;
        }
    }
    return EachCons();
}

@("eachCons") unittest
{
    [1,2,3].eachCons(2).array.should == [[1, 2], [2, 3]];
    [1,2,3].eachCons(4).empty.should == true;
    [1,2,3].eachCons(4).array.empty.should == true;
}

void dfs(I)(void delegate(ref I[]) initial, void delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    queue.initial;
    while (!queue.empty)
    {
        auto i = queue.back;
        queue.popBack;
        i.visit(queue);
        i.visited;
    }
}

void bfs(I)(void delegate(ref I[]) initial, bool delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    initial(queue);
    while (!queue.empty)
    {
        auto i = queue.front;
        queue.popFront;
        bool found = visit(i, queue);
        visited(i);
        if (found)
        {
            break;
        }
    }
}
