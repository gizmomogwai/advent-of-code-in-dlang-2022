import std;

version (unittest)
{
    import unit_threaded;
}

auto exampleData()
{
    return `1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet`;
}

auto exampleData2()
{
    return `two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen`;
}

int findFirstNumber(string input)
{
    for (int i=0; i<input.length; ++i)
    {
        auto h = input[i..$];
        if  (h.startsWith("1") ||
             h.startsWith("one"))
            return 1;
        if  (h.startsWith("2") ||
             h.startsWith("two"))
            return 2;
        if  (h.startsWith("3") ||
             h.startsWith("three"))
            return 3;
        if  (h.startsWith("4") ||
             h.startsWith("four"))
            return 4;
        if  (h.startsWith("5") ||
             h.startsWith("five"))
            return 5;
        if  (h.startsWith("6") ||
             h.startsWith("six"))
            return 6;
        if  (h.startsWith("7") ||
             h.startsWith("seven"))
            return 7;
        if  (h.startsWith("8") ||
             h.startsWith("eight"))
            return 8;
        if  (h.startsWith("9") ||
             h.startsWith("nine"))
            return 9;
    }
    throw new Exception("nothing found in " ~ input);
}
int findLastNumber(string input)
{
    for (int i=(cast(int)input.length)-1; i>=0; --i)
    {
        auto h = input[i..$];
        if  (h.startsWith("1") ||
             h.startsWith("one"))
            return 1;
        if  (h.startsWith("2") ||
             h.startsWith("two"))
            return 2;
        if  (h.startsWith("3") ||
             h.startsWith("three"))
            return 3;
        if  (h.startsWith("4") ||
             h.startsWith("four"))
            return 4;
        if  (h.startsWith("5") ||
             h.startsWith("five"))
            return 5;
        if  (h.startsWith("6") ||
             h.startsWith("six"))
            return 6;
        if  (h.startsWith("7") ||
             h.startsWith("seven"))
            return 7;
        if  (h.startsWith("8") ||
             h.startsWith("eight"))
            return 8;
        if  (h.startsWith("9") ||
             h.startsWith("nine"))
            return 9;
    }
    throw new Exception("nothing found");
}

int addCalibrationValues1(string input)
{
    return input
        .split("\n")
        .filter!("!a.empty")
        .map!(
          line => line
            .filter!(aChar => aChar.isDigit)
            .map!(aChar => aChar - 48)
            .array
        )
        .map!(ints => ints[0] * 10 + ints[$-1])
        .sum;
}

auto realData()
{
    return File("input.txt").readFully;
}

@("example") unittest
{
    exampleData.addCalibrationValues1.should == 142;
    exampleData2.addCalibrationValues2.should == 281;
}

int addCalibrationValues2(string input)
{
    return input
        .split("\n")
        .filter!("!a.empty")
        .map!(
          line => format!("%s%s")(line.findFirstNumber, line.findLastNumber).to!int)
        .sum
    ;
}
@("real") unittest
{
    import std.stdio : writeln;

    writeln("part1: %s".format(realData.addCalibrationValues1));
    writeln("part2: %s".format(realData.addCalibrationValues2));
}

string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

auto eachCons(R)(R range, size_t length)
{
    struct EachCons
    {
        bool empty()
        {
            return range.length < length;
        }
        auto front()
        {
            return range[0..length];
        }
        void popFront()
        {
            range.popFront;
        }
    }
    return EachCons();
}

@("eachCons") unittest
{
    [1,2,3].eachCons(2).array.should == [[1, 2], [2, 3]];
    [1,2,3].eachCons(4).empty.should == true;
    [1,2,3].eachCons(4).array.empty.should == true;
}

void dfs(I)(void delegate(ref I[]) initial, void delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    queue.initial;
    while (!queue.empty)
    {
        auto i = queue.back;
        queue.popBack;
        i.visit(queue);
        i.visited;
    }
}

void bfs(I)(void delegate(ref I[]) initial, bool delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    initial(queue);
    while (!queue.empty)
    {
        auto i = queue.front;
        queue.popFront;
        bool found = visit(i, queue);
        visited(i);
        if (found)
        {
            break;
        }
    }
}
