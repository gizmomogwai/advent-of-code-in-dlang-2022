import std;

version (unittest)
{
    import unit_threaded;
}

auto exampleData()
{
    return `Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green`;
}

auto realData()
{
    return File("input.txt").readFully;
}

struct Draw
{
    string color;
    int count;
}

Draw parseDraw(string input)
{
    auto parts = input.split(" ");
    Draw result;
    result.count = parts[0].to!int;
    result.color = parts[1];
    return result;
}

struct DrawSet
{
    Draw[] draws;
    bool possible(int[string] constraints)
    {
        return draws.all!(draw => draw.count <= constraints[draw.color]);
    }
}

DrawSet parseDrawSet(string input)
{
    DrawSet result;
    result.draws = input.split(", ").map!(draw => draw.parseDraw()).array;
    return result;
}

struct Game
{
    int id;
    DrawSet[] drawSets;
    void parse(string line)
    {
        auto match = line.matchFirst(regex(`Game (?P<id>\d+): (?P<rounds>.+)`));
        id = match["id"].to!int;
        drawSets = match["rounds"].split("; ").map!(drawSet => parseDrawSet(drawSet)).array;
    }
    bool possible(int[string] constraints)
    {
        return drawSets.all!(i => i.possible(constraints));
    }
    int[string] minCubes()
    {
        int[string] result;
        foreach (drawSet; drawSets)
        {
            foreach (draw; drawSet.draws)
            {
                result[draw.color] = max(draw.color in result ? result[draw.color] : 0, draw.count);
            }
        }
        return result;
    }
}

Game[] parseAll(string input)
{
    return input.split("\n").filter!(line => !line.empty).map!((line) { Game g; g.parse(line); return g;}).array;
}

@("example") unittest
{
    auto games = exampleData.parseAll;
    games.length.should == 5;
    games.filter!(i => i.possible(part1Constraints)).array.length.should == 3;

    games.part2.should == 2286;
}

auto part1Constraints()
{
    int[string] result;
    result["red"] = 12;
    result["green"] = 13;
    result["blue"] = 14;
    return result;
}

auto part1(T)(T input)
{
    return input.filter!(i => i.possible(part1Constraints)).map!(i => i.id).sum;
}

auto part2(T)(T input)
{
    return input.map!(game => game.minCubes).map!(cubes => cubes["red"] * cubes["green"] * cubes["blue"]).sum;
}

@("real") unittest
{
    import std.stdio : writeln;

    writeln("part1: %s".format(realData.parseAll.part1));
    writeln("part2: %s".format(realData.parseAll.part2));
}

string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

auto eachCons(R)(R range, size_t length)
{
    struct EachCons
    {
        bool empty()
        {
            return range.length < length;
        }
        auto front()
        {
            return range[0..length];
        }
        void popFront()
        {
            range.popFront;
        }
    }
    return EachCons();
}

@("eachCons") unittest
{
    [1,2,3].eachCons(2).array.should == [[1, 2], [2, 3]];
    [1,2,3].eachCons(4).empty.should == true;
    [1,2,3].eachCons(4).array.empty.should == true;
}

void dfs(I)(void delegate(ref I[]) initial, void delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    queue.initial;
    while (!queue.empty)
    {
        auto i = queue.back;
        queue.popBack;
        i.visit(queue);
        i.visited;
    }
}

void bfs(I)(void delegate(ref I[]) initial, bool delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    initial(queue);
    while (!queue.empty)
    {
        auto i = queue.front;
        queue.popFront;
        bool found = visit(i, queue);
        visited(i);
        if (found)
        {
            break;
        }
    }
}
