import std;

version (unittest)
{
    import unit_threaded;
}

auto exampleInput()
{
    return `467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..`;
}

auto realInput()
{
    return File("input.txt").readFully;
}

struct Number {
    string s;
    int row;
    int start;
    int end;
    int value;
    bool isPart(bool[int][int] symbolsNearby)
    {
        for (int col=start; col<end; col++)
        {
            if (symbolsNearby.require(row).require(col)) return true;
        }
        return false;
    }
    bool closeTo(int row, int column)
    {
        for (int col=start; col<end; col++)
        {
            if ((std.math.algebraic.abs(this.row-row) <= 1)
                && (std.math.algebraic.abs(col-column) <= 1))
            {
                return true;
            }
        }
        return false;
    }
}
Number[] findNumbers(int row, string input)
{
    Number[] result;
    string s;
    int start;
    foreach (column, aChar; input ~ ".")
    {
        if (aChar.isDigit)
        {
            if (s.empty)
            {
                start = cast(int)column;
            }
            s ~= aChar;
        }
        else
        {
            if (!s.empty)
            {
                result ~= Number(s, row, start, cast(int)column, s.to!int);
                s = "";
            }
        }
    }
    return result;
}
auto parse(string input)
{
    Number[] numbers;
    bool[int][int] symbolNearby;
    bool[int][int] gears;
    foreach (row, line; input.split("\n"))
    {
        foreach (col, aChar; line)
        {
            if ((!aChar.isDigit) && (aChar != '.'))
                for (int i=-1; i<2; ++i)
                  for (int j=-1; j<2; ++j)
                      symbolNearby[(cast(int)row)+i][(cast(int)col)+j] = true;
            if (aChar == '*')
                gears[cast(int)row][cast(int)col] = true;
        }
        numbers ~= findNumbers(cast(int)row, line);
    }
    return tuple(numbers.filter!(number => number.isPart(symbolNearby)).array, gears);
}

auto part1(T)(T input)
{
    return input[0].map!(number => number.value).sum;
}

@("part1") unittest
{
    exampleInput.parse.part1.should == 4361;
}

auto part2(T)(T input)
{
    int result = 0;
    foreach (row, v; input[1])
    {
        foreach (col, _; v)
        {
            auto closeParts = input[0].filter!(part => part.closeTo(row, col)).array;
            if (closeParts.length == 2)
            {
                result += closeParts[0].value * closeParts[1].value;
            }
        }
    }
    return result;
}

@("part2") unittest
{
    exampleInput.parse.part2.should == 467835;
}

@("real") unittest
{
    import std.stdio : writeln;

    writeln("part1: %s".format(realInput.parse.part1));
    writeln("part2: %s".format(realInput.parse.part2));
}

string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

auto eachCons(R)(R range, size_t length)
{
    struct EachCons
    {
        bool empty()
        {
            return range.length < length;
        }
        auto front()
        {
            return range[0..length];
        }
        void popFront()
        {
            range.popFront;
        }
    }
    return EachCons();
}

@("eachCons") unittest
{
    [1,2,3].eachCons(2).array.should == [[1, 2], [2, 3]];
    [1,2,3].eachCons(4).empty.should == true;
    [1,2,3].eachCons(4).array.empty.should == true;
}

void dfs(I)(void delegate(ref I[]) initial, void delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    queue.initial;
    while (!queue.empty)
    {
        auto i = queue.back;
        queue.popBack;
        i.visit(queue);
        i.visited;
    }
}

void bfs(I)(void delegate(ref I[]) initial, bool delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    initial(queue);
    while (!queue.empty)
    {
        auto i = queue.front;
        queue.popFront;
        bool found = visit(i, queue);
        visited(i);
        if (found)
        {
            break;
        }
    }
}
