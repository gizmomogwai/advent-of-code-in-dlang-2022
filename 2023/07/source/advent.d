import std;

version (unittest)
{
    import unit_threaded;
}

auto exampleInput()
{
    return `32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483
`;
}

auto realInput()
{
    return File("input.txt").readFully;
}

enum HandType
{
    HIGHEST_CARD = 1,
    ONE_PAIR = 2,
    TWO_PAIRS = 3,
    TRIPLE =4,
    FULL_HOUSE= 5,
    QUADRUPLE=6,
    QUINTUPLE=7,
}

class Hand {
    string cards;
    int bid;
    HandType handType;
    bool withJokers;
    this(string hand, bool withJokers=false)
    {
        this.withJokers = withJokers;
        auto h = hand.split(" ");
        bid = h[1].to!int;
        cards = h[0];
        auto histogram = cards.histogram;
        histogram.require('J', 0);
        auto counts = histogram.values.sort.reverse.array;
        if (counts[0] == 5)
        {
            handType = HandType.QUINTUPLE;
        }
        else if (counts[0] == 4)
        {
            handType = HandType.QUADRUPLE;
        }
        else if (counts[0] == 3)
        {
            if (counts[1] == 2)
            {
                handType = HandType.FULL_HOUSE;
            } else {
                handType = HandType.TRIPLE;
            }
        }
        else if (counts[0] == 2)
        {
            if (counts[1] == 2)
            {
                handType = HandType.TWO_PAIRS;
            }
            else
            {
                handType = HandType.ONE_PAIR;
            }
        }
        else
        {
            handType = HandType.HIGHEST_CARD;
        }
        if (this.withJokers)
        {
            with (HandType)
            {
                int nrOfJokers = histogram['J'];
                final switch (handType)
                {
                case HIGHEST_CARD:
                    switch (nrOfJokers)
                    {
                        // not more than one joker possible, otherwise it would already be a pair
                    case 1:
                        handType = ONE_PAIR;
                        break;
                    default:
                        break;
                    }
                    break;
                case ONE_PAIR:
                    switch (nrOfJokers)
                    {
                    case 1:
                    case 2:
                        handType = TRIPLE;
                        break;
                    default:
                        break;
                    }
                    break;
                case TWO_PAIRS:
                    switch (nrOfJokers)
                    {
                    case 1:
                        handType = FULL_HOUSE;
                        break;
                    case 2:
                        handType = QUADRUPLE;
                        break;
                    default:
                        break;
                    }
                    break;
                case TRIPLE:
                    switch (nrOfJokers)
                    {
                    case 1:
                    case 3:
                        handType = QUADRUPLE;
                        break;
                    default:
                        break;
                    }
                    break;
                case FULL_HOUSE:
                    switch (nrOfJokers)
                    {
                    case 2:
                    case 3:
                        handType = QUINTUPLE;
                        break;
                    default:
                        break;
                    }
                    break;
                case QUADRUPLE:
                    switch (nrOfJokers)
                    {
                    case 1:
                    case 4:
                        handType = QUINTUPLE;
                        break;
                    default:
                        break;
                    }
                    break;
                case QUINTUPLE:
                    break;
                }
            }
        }
    }
    override string toString() const
    {
        return "Hand: %s %s %s jokers %s".format(cards, bid, handType, withJokers);
    }
}

int rank(dchar c, bool withJokers=false)
{
    switch (c)
    {
    case '2': return 2;
    case '3': return 3;
    case '4': return 4;
    case '5': return 5;
    case '6': return 6;
    case '7': return 7;
    case '8': return 8;
    case '9': return 9;
    case 'T': return 10;
    case 'J': return withJokers ? 1 : 11;
    case 'Q': return 12;
    case 'K': return 13;
    case 'A': return 14;
    default:
        throw new Exception("Unknown card %s".format(c));
    }
}

bool lessForHandsOfSameType(Hand hand1, Hand hand2)
{
    foreach (ab; hand1.cards.zip(hand2.cards))
    {
        auto delta = ab[0].rank(hand1.withJokers) - ab[1].rank(hand1.withJokers);
        if (delta == 0)
        {
            continue;
        }
        return delta < 0;
    }
    return false;
}

@("lessForhandsOfSameType") unittest {
    lessForHandsOfSameType(new Hand("23456 10"), new Hand("23567 11")).should == true;
}

auto parse(string input, bool withJokers=false)
{
    return input.split("\n").filter!(l => !l.empty).map!(line => new Hand(line, withJokers)).array;
}

bool compareHands(Hand h1, Hand h2)
{
    if (h1.handType == h2.handType)
    {
        return lessForHandsOfSameType(h1, h2);
    }
    return h1.handType.to!int < h2.handType.to!int;
}
auto part1(T)(T input)
{
    return input
        .multiSort!((a, b) { return compareHands(a, b); }, SwapStrategy.stable)
        .zip(iota(1, input.length+1))
        .array
        .fold!((result, hand) { return hand[0].bid * hand[1] + result; })(0L)
        ;
}

@("part1") unittest
{
    auto hands = exampleInput.parse;
    hands.length.should == 5;

    hands[0].handType.should == HandType.ONE_PAIR;
    hands[1].handType.should == HandType.TRIPLE;
    hands[2].handType.should == HandType.TWO_PAIRS;
    hands[3].handType.should == HandType.TWO_PAIRS;
    hands[4].handType.should == HandType.TRIPLE;

    exampleInput.parse.part1.should == 6440;
}

@("part2") unittest
{
    auto hands = exampleInput.parse(true);
    hands.length.should == 5;

    hands[0].handType.should == HandType.ONE_PAIR;
    hands[1].handType.should == HandType.QUADRUPLE;
    hands[2].handType.should == HandType.TWO_PAIRS;
    hands[3].handType.should == HandType.QUADRUPLE;
    hands[4].handType.should == HandType.QUADRUPLE;

    exampleInput.parse(true).part1.should == 5905;
}

@("real") unittest
{
    import std.stdio : writeln;

    writeln("part1: %s".format(realInput.parse.part1));
    writeln("part2: %s".format(realInput.parse(true).part1));
}

string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

auto eachCons(R)(R range, size_t length)
{
    struct EachCons
    {
        bool empty()
        {
            return range.length < length;
        }
        auto front()
        {
            return range[0..length];
        }
        void popFront()
        {
            range.popFront;
        }
    }
    return EachCons();
}

@("eachCons") unittest
{
    [1,2,3].eachCons(2).array.should == [[1, 2], [2, 3]];
    [1,2,3].eachCons(4).empty.should == true;
    [1,2,3].eachCons(4).array.empty.should == true;
}

void dfs(I)(void delegate(ref I[]) initial, void delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    queue.initial;
    while (!queue.empty)
    {
        auto i = queue.back;
        queue.popBack;
        i.visit(queue);
        i.visited;
    }
}

void bfs(I)(void delegate(ref I[]) initial, bool delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    initial(queue);
    while (!queue.empty)
    {
        auto i = queue.front;
        queue.popFront;
        bool found = visit(i, queue);
        visited(i);
        if (found)
        {
            break;
        }
    }
}

auto histogram(T)(T[] values)
{
    int[T] result;
    foreach (v; values)
    {
        result[v] = result.require(v, 0) + 1;
    }
    return result;
}

@("histogram") unittest {
    auto h = [10, 20, 20, 30, 30, 30, 40, 40, 40, 40].histogram;
    h.values.sort.should == [1, 2, 3, 4];
}
