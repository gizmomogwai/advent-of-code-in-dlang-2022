import std;

version (unittest)
{
    import unit_threaded;
}

auto exampleInput()
{
    return `Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
`;
}

auto realInput()
{
    return File("input.txt").readFully;
}

struct Card
{
    int id;
    bool[int] winningNumbers;
    int[] numbers;
    int count;
    int wins() => cast(int)numbers.count!(n => n in winningNumbers);
}
auto parseCard(string line)
{
    Card result;
    result.count = 1;
    auto cardAndNumbers = line.split(": ").array;
    result.id = cardAndNumbers[0].split(regex(`\s+`))[1].to!int;
    auto winningAndActualNumbers = cardAndNumbers[1].split(regex(`\s+\|\s+`)).array;
    result.winningNumbers = winningAndActualNumbers[0].strip.split(regex(`\s+`)).fold!((result, i) {result[i.to!int] = true; return result;})(result.winningNumbers);
    result.numbers = winningAndActualNumbers[1].strip.split(regex(`\s+`)).map!("a.to!int").array;
    return result;
}

auto parse(string input)
{
    return input.split("\n").filter!(s => !s.empty).map!(line => parseCard(line)).array;
}

auto part1(T)(T input)
{
    return input.fold!(
      (result, i) {
          auto count = i.wins;
          if (count > 0) {
            result += pow(2, count-1);
          }
          return result;
      })(0);
}

@("part1") unittest
{
    exampleInput.parse.part1.should == 13;
}

auto part2(T)(T input)
{
    foreach (idx, card; input)
    {
        auto count = card.wins;
        if (idx < input.length-1) {
            foreach (ref h; input[idx+1..min($, idx+1+count)]) {
                h.count += card.count;
            }
        }
    }
    return input.map!(card => card.count).sum;
}

@("part2") unittest
{
    exampleInput.parse.part2.should == 30;
}

@("real") unittest
{
    import std.stdio : writeln;
    writeln("part1: %s".format(realInput.parse.part1));
    writeln("part2: %s".format(realInput.parse.part2));
}

string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

auto eachCons(R)(R range, size_t length)
{
    struct EachCons
    {
        bool empty()
        {
            return range.length < length;
        }
        auto front()
        {
            return range[0..length];
        }
        void popFront()
        {
            range.popFront;
        }
    }
    return EachCons();
}

@("eachCons") unittest
{
    [1,2,3].eachCons(2).array.should == [[1, 2], [2, 3]];
    [1,2,3].eachCons(4).empty.should == true;
    [1,2,3].eachCons(4).array.empty.should == true;
}

void dfs(I)(void delegate(ref I[]) initial, void delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    queue.initial;
    while (!queue.empty)
    {
        auto i = queue.back;
        queue.popBack;
        i.visit(queue);
        i.visited;
    }
}

void bfs(I)(void delegate(ref I[]) initial, bool delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    initial(queue);
    while (!queue.empty)
    {
        auto i = queue.front;
        queue.popFront;
        bool found = visit(i, queue);
        visited(i);
        if (found)
        {
            break;
        }
    }
}
