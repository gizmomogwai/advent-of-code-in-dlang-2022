import std;

version (unittest)
{
    import unit_threaded;
}

auto exampleInput()
{
    return `0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45
`;
}

auto realInput()
{
    return File("input.txt").readFully;
}

auto parse(string input)
{
    return input.split("\n").filter!(line => !line.empty).map!(line => line.split.map!(i => i.to!long).array).array;
}

auto part1(T)(T input)
{
    long sum = 0;
    foreach (history; input)
    {
        long[][] pyramid;
        pyramid ~= history;
        while (!pyramid[$-1].all!(i => i == 0))
        {
            long[] newLine;
            for (int i=0; i<pyramid[$-1].length-1; ++i)
            {
                newLine ~= pyramid[$-1][i+1] - pyramid[$-1][i];
            }
            pyramid ~= newLine;
        }
        pyramid[$-1] ~= 0;
        for (int i=(cast(int)pyramid.length)-2; i>=0; i--)
        {
            pyramid[i] ~= pyramid[i][$-1] + pyramid[i+1][$-1];
        }
        sum += pyramid[0][$-1];
    }
    return sum;
}

@("part1") unittest
{
    exampleInput.parse[0].should == [0, 3, 6, 9, 12, 15];
    exampleInput.parse[1].should == [1, 3, 6, 10, 15, 21];
    exampleInput.parse[2].should == [10, 13, 16, 21, 30, 45];
    exampleInput.parse.part1.should == 114;
}

auto part2(T)(T input)
{
    long sum = 0;
    foreach (history; input)
    {
        long[][] pyramid;
        pyramid ~= history;
        while (!pyramid[$-1].all!(i => i == 0))
        {
            long[] newLine;
            for (int i=0; i<pyramid[$-1].length-1; ++i)
            {
                newLine ~= pyramid[$-1][i+1] - pyramid[$-1][i];
            }
            pyramid ~= newLine;
        }
        pyramid[$-1] = [0L] ~ pyramid[$-1];
        //bw      writeln(pyramid);
        for (int i=(cast(int)pyramid.length)-2; i>=0; i--)
        {
            pyramid[i] = [pyramid[i][0] - pyramid[i+1][0]] ~ pyramid[i];
        }
//        writeln(pyramid);
        sum = sum + pyramid[0][0];
    }
    return sum;
}

@("part2") unittest
{
    exampleInput.parse.part2.should == 2;
}

@("real") unittest
{
    import std.stdio : writeln;

    writeln("part1: %s".format(realInput.parse.part1));
    writeln("part2: %s".format(realInput.parse.part2));
}

string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

auto eachCons(R)(R range, size_t length)
{
    struct EachCons
    {
        bool empty()
        {
            return range.length < length;
        }
        auto front()
        {
            return range[0..length];
        }
        void popFront()
        {
            range.popFront;
        }
    }
    return EachCons();
}

@("eachCons") unittest
{
    [1,2,3].eachCons(2).array.should == [[1, 2], [2, 3]];
    [1,2,3].eachCons(4).empty.should == true;
    [1,2,3].eachCons(4).array.empty.should == true;
}

void dfs(I)(void delegate(ref I[]) initial, void delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    queue.initial;
    while (!queue.empty)
    {
        auto i = queue.back;
        queue.popBack;
        i.visit(queue);
        i.visited;
    }
}

void bfs(I)(void delegate(ref I[]) initial, bool delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    initial(queue);
    while (!queue.empty)
    {
        auto i = queue.front;
        queue.popFront;
        bool found = visit(i, queue);
        visited(i);
        if (found)
        {
            break;
        }
    }
}

long greatestCommonDivisor(long a, long b)
{
    if (a < b)
    {
        swap(a, b);
    }
    while (b != 0)
    {
        a = a % b;
        swap(a, b);
    }
    return a;
}

long leastCommonMultiple(long a, long b)
{
    return a / greatestCommonDivisor(a, b) * b;
}
