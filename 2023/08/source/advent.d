import std;

version (unittest)
{
    import unit_threaded;
}

auto exampleInput()
{
    return `RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)
`;
}

auto realInput()
{
    return File("input.txt").readFully;
}

class Network {
    string moves;
    string[2][string] edges;
}
auto parse(string input)
{
    Network result = new Network;
    auto lines = input.split("\n").filter!(line => !line.empty).array;
    result.moves = lines[0];
    foreach (line; lines[1..$])
    {
        string from;
        string[2] to;
        line.formattedRead("%s = (%s, %s)", from, to[0], to[1]);
        result.edges[from] = to;
    }
    return result;
}

auto part1(T)(T network)
{
    string position = "AAA";
    int n = 0;
    foreach (move; network.moves.cycle)
    {
        position = network.edges[position][move == 'L' ? 0 : 1];
        n++;
        if (position == "ZZZ")
        {
            break;
        }
    }
    return n;
}

@("part1") unittest
{
    exampleInput.parse.part1.should == 2;
    `LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)`.parse.part1.should == 6;
}

auto part2(T)(T network)
{
    struct State {
        string node;
        int index;
    }

    string[] startNodes = network.edges.keys.filter!(node => node[2] == 'A').array;

    long result = 1;
    foreach (startNode; startNodes)
    {
        auto moves = network.moves;
        auto moveIndex = 0;
        auto position = startNode;
        int[State] states;
        states[State(position, 0)] = 1;

        State state;
        State lastState;
        while (true)
        {
            auto move = moves[moveIndex];
            position = network.edges[position][move == 'L' ? 0 : 1];
            lastState = state;
            state = State(position, moveIndex);
            if (state in states)
            {
                if (states[state] == 2)
                {
                    break;
                }
                else
                {
                    states[state] = states[state]+1;
                }
            }
            else
            {
                states[state] = 1;
            }
            moveIndex = cast(int)((moveIndex + 1) % moves.length);
        }
        assert(lastState[2] == 'Z');
        auto h = states.length-states.values.filter!(v => v == 1).count;
        result = leastCommonMultiple(result, h);
    }
    return result;
}

@("part2") unittest
{
    `LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)`.parse.part2.should == 6;
}

@("real") unittest
{
    import std.stdio : writeln;

    writeln("part1: %s".format(realInput.parse.part1));
    writeln("part2: %s".format(realInput.parse.part2));
}

string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

auto eachCons(R)(R range, size_t length)
{
    struct EachCons
    {
        bool empty()
        {
            return range.length < length;
        }
        auto front()
        {
            return range[0..length];
        }
        void popFront()
        {
            range.popFront;
        }
    }
    return EachCons();
}

@("eachCons") unittest
{
    [1,2,3].eachCons(2).array.should == [[1, 2], [2, 3]];
    [1,2,3].eachCons(4).empty.should == true;
    [1,2,3].eachCons(4).array.empty.should == true;
}

void dfs(I)(void delegate(ref I[]) initial, void delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    queue.initial;
    while (!queue.empty)
    {
        auto i = queue.back;
        queue.popBack;
        i.visit(queue);
        i.visited;
    }
}

void bfs(I)(void delegate(ref I[]) initial, bool delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    initial(queue);
    while (!queue.empty)
    {
        auto i = queue.front;
        queue.popFront;
        bool found = visit(i, queue);
        visited(i);
        if (found)
        {
            break;
        }
    }
}

long greatestCommonDivisor(long a, long b)
{
    if (a < b)
    {
        swap(a, b);
    }
    while (b != 0)
    {
        a = a % b;
        swap(a, b);
    }
    return a;
}

long leastCommonMultiple(long a, long b)
{
    return a / greatestCommonDivisor(a, b) * b;
}
