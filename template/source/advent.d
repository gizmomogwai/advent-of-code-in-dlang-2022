import std;

version (unittest)
{
    import unit_threaded;
}

auto exampleInput()
{
    return ``;
}

auto realInput()
{
    return File("input.txt").readFully;
}

auto parse(string input)
{
    return "TODO";
}

auto part1(T)(T input)
{
    return "TODO";
}

@("part1") unittest
{
    true.should == false;
}

auto part2(T)(T input)
{
    return "TODO";
}

@("part2") unittest
{
    true.should == false;
}

@("real") unittest
{
    import std.stdio : writeln;

    writeln("part1: %s".format(realInput.parse.part1));

    writeln("part2: %s".format(realInput.parse.part2));
}

string readFully(File f)
{
    char[] buffer = new char[4096];
    auto result = appender!string;
    auto r = f.rawRead(buffer);
    while (r.length > 0)
    {
        result.put(r);
        r = f.rawRead(buffer);
    }
    return result.data.to!string;
}

auto eachCons(R)(R range, size_t length)
{
    struct EachCons
    {
        bool empty()
        {
            return range.length < length;
        }
        auto front()
        {
            return range[0..length];
        }
        void popFront()
        {
            range.popFront;
        }
    }
    return EachCons();
}

@("eachCons") unittest
{
    [1,2,3].eachCons(2).array.should == [[1, 2], [2, 3]];
    [1,2,3].eachCons(4).empty.should == true;
    [1,2,3].eachCons(4).array.empty.should == true;
}

void dfs(I)(void delegate(ref I[]) initial, void delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    queue.initial;
    while (!queue.empty)
    {
        auto i = queue.back;
        queue.popBack;
        i.visit(queue);
        i.visited;
    }
}

void bfs(I)(void delegate(ref I[]) initial, bool delegate(ref I, ref I[]) visit, void delegate(ref I) visited)
{
    I[] queue;
    initial(queue);
    while (!queue.empty)
    {
        auto i = queue.front;
        queue.popFront;
        bool found = visit(i, queue);
        visited(i);
        if (found)
        {
            break;
        }
    }
}

long greatestCommonDivisor(long a, long b)
{
    if (a < b)
    {
        swap(a, b);
    }
    while (b != 0)
    {
        a = a % b;
        swap(a, b);
    }
    return a;
}

long leastCommonMultiple(long a, long b)
{
    return a / greatestCommonDivisor(a, b) * b;
}
