YEAR = 2023

today = Time.now.day
today_dir = "#{YEAR}/%02d" % today

desc "Get input of today (#{today})"
task :get_input, [:day] do |t, args|
    day = args[:day] || today
    d = ("#{YEAR}/%02d" % day) || today_dir
    cookies = File.read("cookies.txt")
    output = File.join(d, "input.txt")
    sh "mkdir -p #{File.dirname(output)}"
    sh "http -pb --output=#{output} https://adventofcode.com/#{YEAR}/day/#{day}/input #{cookies}"
end

desc "Run build for #{today}, or the day specified in []"
task :run, [:day] do |t, args|
    d = args[:day] || today_dir
    sh "rsync --archive --ignore-existing template/ #{d}"
    cd d do
        sh 'watchexec --restart --filter=dub.sdl --filter="*.d" --filter=input.txt "d test"'
    end
end

task :default => [:get_input]
